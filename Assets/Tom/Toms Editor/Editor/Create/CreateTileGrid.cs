﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class CreateTileGrid : ScriptableWizard {

	public string tname = "Tile";
	public bool nameSequencial = true;
	public bool isPerspective = true;
	public GameObject Tile;
	public Transform Parent;
	public float yOffset = 1.1f;
	public float xOffset = 2.1f;
	public int numRows = 5;
	public int numColums = 5;
	public Transform[,] tileGrid;

	[MenuItem ("Toms Editor/Create/Tile Grid #t")]
	
	
	static void CreateWizard ()
	{
		ScriptableWizard.DisplayWizard <CreateTileGrid>("Create Grid", "Create");
		
		
	}
	
	void OnWizardCreate ()
	{

		tileGrid = new Transform[numColums,numRows];
		for (int i = 0; i < numColums; i++) {
			
			for (int j = 0; j < numRows; j++){
				
				GameObject c = (GameObject)PrefabUtility.InstantiatePrefab(Tile);
				Transform clone = c.transform;
				Vector3 v0 = Vector3.zero;
				if (isPerspective)
					v0 = new Vector3((xOffset * i) + (xOffset *  j), (yOffset * j) -(yOffset * i));
				else
					v0 = new Vector3(xOffset * i,yOffset * j);

				clone.position = v0;
				clone.parent = Parent;
				if (nameSequencial)
					clone.name = tname + i.ToString() + j.ToString();
				else
					clone.name = tname;
				
				tileGrid[i,j] = clone;
			}
		}

	}

}
