﻿using UnityEngine;
using UnityEditor;    

public class CreateAsChild : MonoBehaviour {
	
	[MenuItem("Toms Editor/Create/Create Empty Parent")]
	static void createEmptyParent() {
		GameObject go = new GameObject("GameObject");
		if(Selection.activeTransform != null)
		{
			
			go.transform.parent = Selection.activeTransform.parent;
			
			go.transform.Translate(Selection.activeTransform.position);
			
			Selection.activeTransform.parent = go.transform;
			
		}
		
	}  
	[MenuItem("Toms Editor/Destroy/Destroy All Children")]
	static void DestroyChildren() {
		
		if(Selection.activeTransform != null)
		{
			while (Selection.activeTransform.childCount > 0){
				foreach (Transform t in Selection.activeTransform){
					DestroyImmediate(t.gameObject);
				}
			}
		}
		
	}
	
	[MenuItem("Toms Editor/Create/Create Empty Duplicate")]
	static void createEmptyDuplicate() {
		
		GameObject go = new GameObject("GameObject");
		
		if(Selection.activeTransform != null)
		{
			go.transform.parent = Selection.activeTransform.parent;
			go.transform.Translate(Selection.activeTransform.position);
		}
		
	}
	
	[MenuItem("Toms Editor/Create/Create Empty Child")]
	static void createEmptyChild() {
		
		GameObject go = new GameObject("GameObject");
		
		if(Selection.activeTransform != null)
		{
			go.transform.parent = Selection.activeTransform;
			go.transform.Translate(Selection.activeTransform.position);
		}
		
	}
	
}
