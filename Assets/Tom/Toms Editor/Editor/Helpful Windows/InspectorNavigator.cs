﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEditor;
using System;
using System.Collections;

namespace TomsEditor
{
    public class InspectorNavigator : EditorWindow, IHasCustomMenu
    {
        public const int CurrentVersion = 103;
        public const int MaxEnqueuedElements = 256;

        static InspectorNavigator Instance;

        static GUIStyle LabelStyle;
        static Texture2D prevBtn;
        static Texture2D nextBtn;
        static GUIContent gc = new GUIContent();
        static Texture2D grayLabel;
        static Texture2D greenLabel;
        static Texture2D redLabel;

        [SerializeField]
        public List<int> Back = new List<int>();
        [SerializeField]
        public List<int> Next = new List<int>();

        int CurrentSelection;
        bool IsInspectorLocked;

        System.Type InspectorWindow;
        IEnumerable InspectorList;

        [MenuItem("Toms Editor/Tools/Navigator/Open Navigator")]
        public static void OpenWindow()
        {
            var ins = GetWindow<InspectorNavigator>("Toms Navigator", false, new System.Type[] { InspectorWindowType() });
            if (ins != null) Instance = ins;
        }

        
		[MenuItem("Toms Editor/Tools/Navigator/Back %LEFT")]
        public static void DoBackCommand()
        {
            if (Instance == null) OpenWindow();
            if (Instance == null) return;
            Instance.UnlockInspector();
            Instance.DoBack();
            Instance.Repaint();
        }

     
		[MenuItem("Toms Editor/Tools/Navigator/Forward %RIGHT")]
        public static void DoNextCommand()
        {
            if (Instance == null) OpenWindow();
            if (Instance == null) return;
            Instance.UnlockInspector();
            Instance.DoNext();
            Instance.Repaint();
        }

		[MenuItem("Toms Editor/Tools/Navigator/Clear History")]
        public static void ClearBreadcrumbs()
        {
            if (Instance == null) OpenWindow();
            if (Instance == null) return;
            Instance.Back.Clear();
            Instance.Next.Clear();
            Instance.CurrentSelection = 0;
            Instance.Repaint();
        }


        void OnEnable()
        {
         
            minSize = maxSize = new Vector2(300, 22);
            if (Back.Count > MaxEnqueuedElements) Back.RemoveRange(0, Back.Count - MaxEnqueuedElements);
            if (Next.Count > MaxEnqueuedElements) Next.RemoveRange(0, Next.Count - MaxEnqueuedElements);
            if (Back.Count > 0)
            {
                CurrentSelection = Back[Back.Count - 1];
                Back.RemoveAt(Back.Count - 1);
            }
            GetInspectorList();
        }

        void OnGUI()
        {
            Preloads();

            CheckIfInspectorIsLocked();

            EditorGUILayout.BeginHorizontal();

            bool bNext = false;
            int iBack = 0;
            Rect rect = GetControlRect(Screen.width - 90);
            rect.y += 2;
            rect.x += rect.width;
            while (Next.Count > 0)
            {
                try
                {
                    string name = EditorUtility.InstanceIDToObject(Next[Next.Count - 1]).name;
                    if (DrawLabel(name, ref rect, grayLabel))
                    {
                        if (IsInspectorLocked) Instance.UnlockInspector();
                        bNext = true;
                    }
                    break;
                }
                catch
                {
                    Next.RemoveAt(Next.Count - 1);
                }
            }

            if (CurrentSelection != 0)
            {
                UnityEngine.Object obj = EditorUtility.InstanceIDToObject(CurrentSelection);
                if (obj != null)
                {
                    if (DrawLabel(obj.name, ref rect, greenLabel))
                        Selection.activeObject = obj;
                }
            }

            for (int i = Back.Count - 1; i >= 0; i--)
            {
                if (rect.x > 0)
                {
                    try
                    {
                        string name = EditorUtility.InstanceIDToObject(Back[i]).name;
                        if (DrawLabel(name, ref rect, grayLabel))
                        {
                            if (IsInspectorLocked) Instance.UnlockInspector();
                            iBack = Back.Count - i;
                        }
                    }
                    catch
                    {
                        Back.RemoveAt(i);
                    }
                }
                else
                    break;
            }

            EditorGUILayout.BeginHorizontal(GUILayout.Width(80));

            GUI.enabled = Back.Count > 0;
            if (GUILayout.Button(prevBtn))
            {
                if (IsInspectorLocked) UnlockInspector();
                DoBack();
            }

            GUI.enabled = Next.Count > 0;
            if (GUILayout.Button(nextBtn))
            {
                if (IsInspectorLocked) UnlockInspector();
                DoNext();
            }
            GUI.enabled = true;

          

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndHorizontal();

            if (bNext) { DoNext(); Repaint(); }
            if (iBack != 0)
            {
                for (int i = 0; i < iBack; ++i)
                    DoBack();
                Repaint();
            }
        }

        void AddCurrentToBack()
        {
            if (CurrentSelection == 0) return;
            if (Back.Count == 0 || Back[Back.Count - 1] != CurrentSelection) Back.Add(CurrentSelection);
        }

        void AddCurrentToNext()
        {
            if (CurrentSelection == 0) return;
            if (Next.Count == 0 || Next[Next.Count - 1] != CurrentSelection) Next.Add(CurrentSelection);
        }

        void DoNext()
        {
            if (IsInspectorLocked) return;
            if (Next.Count > 0)
            {
                AddCurrentToBack();
                CurrentSelection = Next[Next.Count - 1];
                Next.RemoveAt(Next.Count - 1);
                Selection.activeObject = EditorUtility.InstanceIDToObject(CurrentSelection);
            }
        }

        void DoBack()
        {
            if (IsInspectorLocked) return;
            if (Back.Count > 0)
            {
                AddCurrentToNext();
                CurrentSelection = Back[Back.Count - 1];
                Back.RemoveAt(Back.Count - 1);
                Selection.activeObject = EditorUtility.InstanceIDToObject(CurrentSelection);
            }
        }

        void DrawNotificationIcon()
        {
            gc.text = "!";
          
            LabelStyle.fontStyle = FontStyle.Bold;
            LabelStyle.margin.top = 4;
            GUILayout.Label(gc, LabelStyle, GUILayout.Width(16));
            LabelStyle.fontStyle = FontStyle.Normal;
        }

        bool DrawLabel(string text, ref Rect rect, Texture2D color)
        {
            if (string.IsNullOrEmpty(text)) text = "...";
            LabelStyle.normal.background = color;
            gc.text = text;
            Vector2 size = LabelStyle.CalcSize(gc);
            rect.width = size.x;
            rect.x -= size.x;
            GUI.Label(rect, gc, LabelStyle);
            return (Event.current.type == EventType.mouseDown && rect.Contains(Event.current.mousePosition));
        }

        void OnHierarchyChange()
        {
            Repaint();
        }

        void OnProjectChange()
        {
            Repaint();
        }

        void OnSelectionChange()
        {
            if (CheckIfInspectorIsLocked()) return;

            int activeObjectID = Selection.activeObject != null ? Selection.activeObject.GetInstanceID() : 0;
            if (activeObjectID == 0) // Unselect
            {
                if (CurrentSelection != 0)
                {
                    AddCurrentToBack();
                    CurrentSelection = 0;
                    EditorUtility.SetDirty(this);
                    Repaint();
                }
            }
            else if (CurrentSelection != activeObjectID) // New object selected
            {
                if (Back.Count > 0 && Back[Back.Count - 1] == activeObjectID) // Undo/Back
                    DoBack();
                else if (Next.Count > 0 && Next[Next.Count - 1] == activeObjectID) // Redo/Next
                    DoNext();
                else
                {
                    AddCurrentToBack();
                    Next.Clear();
                }

                CurrentSelection = activeObjectID;
                EditorUtility.SetDirty(this);
                Repaint();
            }
        }

        void Update()
        {
           
        }

        void Preloads()
        {
            if (LabelStyle == null)
            {
				LabelStyle = new GUIStyle ()
                {
					alignment = TextAnchor.MiddleCenter,
					font = EditorStyles.miniBoldFont,
                    border = new RectOffset(7, 7, 0, 0),
                    clipping = TextClipping.Clip,
                    fontSize = 0,
                    fixedHeight = 15,
                    margin = new RectOffset(3, 3, 0, 0),
                    padding = new RectOffset(6, 6, 0, 0)
                };
				LabelStyle.normal.textColor = new Color(0.8f,0.8f,0.8f);
            }

			if (grayLabel == null) 
				grayLabel = (Texture2D)Resources.Load ("Editor Textures/greybtn",typeof(Texture2D));//EditorGUIUtility.Load("greybtn.png") as Texture2D;//IconContent("sv_label_0"); 

			if (greenLabel == null) greenLabel =  (Texture2D)Resources.Load ("Editor Textures/bluebtn",typeof(Texture2D));//EditorGUIUtility.Load("bluebtn.png") as Texture2D;//IconContent("sv_label_2");
			if (redLabel == null) redLabel =  (Texture2D)Resources.Load ("Editor Textures/redbtn",typeof(Texture2D));//EditorGUIUtility.Load("redbtn.png") as Texture2D;//IconContent("sv_label_6");

            if (nextBtn == null || prevBtn == null)
            {
                nextBtn = new Texture2D(4, 4, TextureFormat.ARGB32, false);
                if (!EditorGUIUtility.isProSkin)
                    nextBtn.LoadImage(nextLight);
                else
                    nextBtn.LoadImage(nextDark);
                nextBtn.Apply();

                prevBtn = FlipTexture(nextBtn);
                prevBtn.hideFlags = HideFlags.HideAndDontSave;
            }
        }

        Texture2D FlipTexture(Texture2D original)
        {
            var flipped = new Texture2D(original.width, original.height, original.format, false) { filterMode = original.filterMode };
            var w = original.width;
            var h = original.height;
            for (int y = 0; y < h; y++)
                for (int x = 0; x < w; x++)
                    flipped.SetPixel(w - x - 1, y, original.GetPixel(x, y));
            flipped.Apply();
            return flipped;
        }

        public void AddItemsToMenu(GenericMenu menu)
        {
			menu.AddItem (new GUIContent ("Clear History"), false, new GenericMenu.MenuFunction (() => {
					Back.Clear ();
					Next.Clear ();
					CurrentSelection = 0;
			}));
        }

        #region Hack_Fu

        byte[] nextDark = new byte[]{
            0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D, 0x49, 0x48, 0x44, 0x52,
            0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x0C, 0x08, 0x06, 0x00, 0x00, 0x00, 0x56, 0x75, 0x5C,
            0xE7, 0x00, 0x00, 0x00, 0xFF, 0x49, 0x44, 0x41, 0x54, 0x28, 0x15, 0x63, 0xFC, 0xFF, 0xFF, 0x3F,
            0x03, 0x29, 0x80, 0x89, 0x14, 0xC5, 0x60, 0xB5, 0x20, 0x1B, 0xE4, 0xE4, 0xE4, 0xC4, 0x16, 0x2C,
            0x58, 0xE0, 0x6F, 0x6A, 0x6A, 0xCA, 0x8F, 0xCB, 0x80, 0x2B, 0x57, 0xAE, 0x30, 0x80, 0x5D, 0x03,
            0xD5, 0xA0, 0xF5, 0xE9, 0xD3, 0xA7, 0x99, 0xAF, 0x5E, 0xBD, 0x6A, 0x5F, 0xBE, 0x7C, 0xB9, 0x9B,
            0x9D, 0x9D, 0x9D, 0x20, 0xBA, 0x46, 0x98, 0x06, 0x98, 0x93, 0xC0, 0x34, 0x07, 0x07, 0x87, 0x90,
            0xB7, 0xB7, 0x77, 0xF0, 0xA6, 0x4D, 0x9B, 0x9A, 0xAE, 0x5F, 0xBF, 0xEE, 0x00, 0xD4, 0xC4, 0x88,
            0xAE, 0x11, 0xA6, 0x01, 0x45, 0x9C, 0x89, 0x89, 0x89, 0x4D, 0x5A, 0x5A, 0x3A, 0x12, 0x68, 0x63,
            0xFD, 0xE4, 0xC9, 0x93, 0x6D, 0x90, 0x35, 0x62, 0xD5, 0x00, 0xD3, 0x0D, 0xB4, 0x51, 0xD2, 0xC5,
            0xC5, 0x25, 0x68, 0xCA, 0x94, 0x29, 0x81, 0x40, 0x67, 0x72, 0x80, 0xC4, 0x59, 0x60, 0x92, 0xD8,
            0xE8, 0xA9, 0x53, 0xA7, 0x3E, 0xA8, 0xAC, 0xAC, 0x3C, 0x0C, 0x94, 0xBB, 0x05, 0xC4, 0xBF, 0x41,
            0x6A, 0x30, 0x34, 0x7C, 0xF8, 0xF0, 0xE1, 0xCF, 0x9A, 0x35, 0x6B, 0x9E, 0xCC, 0x9C, 0x39, 0xF3,
            0xEC, 0xCD, 0x9B, 0x37, 0xAF, 0x01, 0xD5, 0xBC, 0x06, 0xE2, 0x5F, 0x40, 0x4F, 0x83, 0x23, 0x0C,
            0xAE, 0xE1, 0xED, 0xDB, 0xB7, 0xBF, 0xB7, 0x6D, 0xDB, 0xF6, 0x02, 0xA8, 0xF0, 0xC2, 0xA5, 0x4B,
            0x97, 0x2E, 0x03, 0x15, 0xBD, 0x04, 0xE2, 0x1F, 0x40, 0x8C, 0x0A, 0xA0, 0x31, 0x2D, 0x0D, 0x14,
            0x75, 0x02, 0x62, 0x05, 0x20, 0x66, 0x07, 0x62, 0x0C, 0x00, 0x0B, 0x56, 0x46, 0x90, 0x86, 0xD5,
            0xAB, 0x57, 0x33, 0x03, 0x55, 0x80, 0xF0, 0x1F, 0x20, 0xFE, 0x87, 0xA1, 0x1A, 0x2A, 0x10, 0x1A,
            0x1A, 0xCA, 0x00, 0x00, 0x37, 0x3C, 0x6A, 0x87, 0x12, 0x9D, 0x81, 0x59, 0x00, 0x00, 0x00, 0x00,
            0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82
        };

        byte[] nextLight = new byte[]{
            0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D, 0x49, 0x48, 0x44, 0x52,
            0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x0C, 0x08, 0x06, 0x00, 0x00, 0x00, 0x56, 0x75, 0x5C,
            0xE7, 0x00, 0x00, 0x00, 0x09, 0x70, 0x48, 0x59, 0x73, 0x00, 0x00, 0x0B, 0x12, 0x00, 0x00, 0x0B,
            0x12, 0x01, 0xD2, 0xDD, 0x7E, 0xFC, 0x00, 0x00, 0x01, 0x0F, 0x49, 0x44, 0x41, 0x54, 0x28, 0x91,
            0x95, 0x90, 0xB1, 0x4A, 0xC3, 0x60, 0x14, 0x85, 0x4F, 0x4B, 0x51, 0x97, 0x60, 0x34, 0x24, 0x20,
            0x2E, 0x6E, 0xA1, 0xC9, 0x4F, 0x9E, 0xC0, 0x41, 0x1C, 0x1C, 0x32, 0x44, 0xAA, 0x21, 0xC4, 0x2D,
            0xDB, 0x3F, 0x24, 0x79, 0x06, 0xC1, 0x25, 0xCF, 0xE0, 0x83, 0x64, 0xCB, 0x92, 0x38, 0x06, 0x41,
            0x30, 0x38, 0x38, 0x99, 0x60, 0xA0, 0xE2, 0x0B, 0xFC, 0xD8, 0x92, 0xEB, 0x60, 0xD5, 0xA0, 0xB5,
            0xD8, 0x0B, 0x17, 0xEE, 0x70, 0x3E, 0xCE, 0x3D, 0x67, 0x40, 0x44, 0x58, 0x67, 0x86, 0x6B, 0xA9,
            0x3F, 0x81, 0xA6, 0x69, 0xB4, 0x20, 0x08, 0x4E, 0xCB, 0xB2, 0xDC, 0xFE, 0x4B, 0xC8, 0x18, 0xFB,
            0x38, 0x88, 0x08, 0x75, 0x5D, 0x1B, 0x92, 0x24, 0x5D, 0xAB, 0xAA, 0x9A, 0xF8, 0xBE, 0x7F, 0x92,
            0xE7, 0xF9, 0x0E, 0x11, 0xA1, 0xBF, 0xA6, 0x69, 0x82, 0x88, 0xBE, 0x5E, 0x1A, 0x02, 0x80, 0x10,
            0x62, 0x37, 0x4D, 0xD3, 0x73, 0xC7, 0x71, 0xAE, 0xC6, 0xE3, 0xF1, 0x11, 0x80, 0xC1, 0xBF, 0x32,
            0x74, 0x5D, 0xB7, 0xD1, 0xB6, 0xED, 0x85, 0xA6, 0x69, 0x97, 0x71, 0x1C, 0x1F, 0xF6, 0xC1, 0x95,
            0xA1, 0x85, 0x10, 0x7B, 0x59, 0x96, 0x9D, 0x45, 0x51, 0x34, 0x29, 0x8A, 0x62, 0x0B, 0x00, 0x46,
            0xAB, 0x80, 0x30, 0x0C, 0x9F, 0x92, 0x24, 0xB9, 0x01, 0xF0, 0x08, 0x60, 0xB6, 0x14, 0x90, 0x65,
            0x79, 0xEE, 0xBA, 0xEE, 0x33, 0xE7, 0xFC, 0x56, 0xD7, 0xF5, 0x07, 0x00, 0xAF, 0x00, 0xDE, 0x18,
            0x63, 0x54, 0x55, 0xD5, 0x37, 0xA0, 0x28, 0xCA, 0xCC, 0xB6, 0xED, 0x29, 0xE7, 0xFC, 0xCE, 0xB2,
            0xAC, 0x7B, 0x00, 0x2F, 0x00, 0xC4, 0x2F, 0xDB, 0x45, 0x6D, 0xFB, 0x44, 0x74, 0x4C, 0x44, 0x07,
            0x44, 0xB4, 0xF9, 0xB3, 0xD2, 0x7E, 0xAD, 0x23, 0x00, 0xF0, 0x3C, 0x6F, 0xBA, 0xB0, 0x9E, 0x03,
            0xE8, 0x96, 0xE5, 0x31, 0x0C, 0x03, 0x00, 0xF0, 0x0E, 0xB0, 0x78, 0x7F, 0xE2, 0x89, 0x17, 0x10,
            0x89, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82
        };

        static System.Type InspectorWindowType()
        {
            return typeof(EditorGUI).Assembly.GetType("UnityEditor.InspectorWindow");
        }

        Texture2D IconContent(string name)
        {
            System.Reflection.MethodInfo mi = typeof(EditorGUIUtility).GetMethod("IconContent", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public, null, new System.Type[] { typeof(string) }, null);
            if (mi == null) mi = typeof(EditorGUIUtility).GetMethod("IconContent", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic, null, new System.Type[] { typeof(string) }, null);
            return (Texture2D)((GUIContent)mi.Invoke(null, new object[] { name })).image;
        }

        Rect GetControlRect(float width)
        {
            System.Reflection.MethodInfo mi = typeof(EditorGUILayout).GetMethod("GetControlRect", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public, null, new System.Type[] { typeof(GUILayoutOption[]) }, null);
            if (mi == null) mi = typeof(EditorGUILayout).GetMethod("GetControlRect", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic, null, new System.Type[] { typeof(GUILayoutOption[]) }, null);
            return (Rect)mi.Invoke(null, new object[] { new GUILayoutOption[] { GUILayout.Width(width) } });
        }

        void GetInspectorList()
        {
            try
            {
                InspectorWindow = typeof(EditorGUI).Assembly.GetType("UnityEditor.InspectorWindow");
                InspectorList = (IEnumerable)InspectorWindow.GetMethod("GetInspectors", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic).Invoke(null, null);
            }
            catch { };
        }

        bool CheckIfInspectorIsLocked()
        {
            try
            {
                var enumerator = InspectorList.GetEnumerator(); 
                enumerator.MoveNext();
                var firstInspectorWindow = enumerator.Current;
                IsInspectorLocked = (bool)InspectorWindow.GetProperty("isLocked", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public).GetValue(firstInspectorWindow, null);
                return IsInspectorLocked;
            }
            catch { };
            return IsInspectorLocked = false;
        }

        void UnlockInspector()
        {
            try
            {
                var enumerator = InspectorList.GetEnumerator();
                enumerator.MoveNext();
                var firstInspectorWindow = enumerator.Current;
                InspectorWindow.GetProperty("isLocked", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public).SetValue(firstInspectorWindow, false, null);
                IsInspectorLocked = false;
            }
            catch { };
        }

        #endregion
    }
}