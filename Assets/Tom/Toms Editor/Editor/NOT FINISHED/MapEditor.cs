﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


public class MapEditor : EditorWindow {

	public Texture2D thomasFerrerTexture;
	string myString = "Hello World";
	public bool groupEnabled;
	public bool myBool = true;
	public float myFloat = 1.23f;

	bool showPosition = true;
	string status = "Tile Sets";
	public Texture2D RoundTileTexture;
	public GameObject RoundTile;
	public Texture2D RoundTopTexture;
	public GameObject RoundTopTile;
	public Texture2D RoundBottomTexture;
	public GameObject RoundBottomTile;
	public Texture2D RoundRightTexture;
	public GameObject RoundRightTile;
	public Texture2D RoundLeftTexture;
	public GameObject RoundLeftTile;
	
	public GameObject TopRightCornerTile;
	
	public GameObject TopLeftCornerTile;
	
	public GameObject BottomRightCornerTile;
	
	public GameObject BottomLeftCornerTile;
	
	public GameObject SideRightLeftTile;
	
	public GameObject SideTopBottomTile;
	
	public GameObject TopTile;
	
	public GameObject BottomTile;
	
	public GameObject RightTile;
	
	public GameObject LeftTile;
	
	public GameObject EmptyTile;
	
	[MenuItem ("Toms Editor/Tools/Tile Map",false,1)]
	public static void ShowWindow(){
		EditorWindow.GetWindow(typeof(MapEditor));
	}

	void OnEnable(){

		thomasFerrerTexture = (Texture2D)Resources.Load ("ThomasFerrer",typeof(Texture2D));
	}

	public void OnGUI(){

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField(new GUIContent(thomasFerrerTexture),GUILayout.Width(64f),GUILayout.Height(64f));
		EditorGUILayout.LabelField ("Toms Editor Tile Map", EditorStyles.boldLabel);
		EditorGUILayout.EndHorizontal();
		myString = EditorGUILayout.TextField ("Text Field", myString);
		
		groupEnabled = EditorGUILayout.Toggle("Optional Settings",groupEnabled);


		if (groupEnabled){
			myBool = EditorGUILayout.Toggle ("Toggle", myBool);
			myFloat = EditorGUILayout.Slider ("Slider", myFloat, -3, 3);
		}

		showPosition = EditorGUILayout.Foldout(showPosition, status);
		if(showPosition){
			status = "Tile Sets";

		}

		if (GUILayout.Button("New Set")){

			//TileSetConfig tileSetConfig = TileSetConfig.NewConfig();

		}
		

//		if (GUILayout.Button("Go to API"))
//			if (source == null)
//				ShowNotification(new GUIContent("No object selected for searching"));
//		else
//			if (Help.HasHelpForObject(source))
//				Help.ShowHelpForObject(source);
//		else
//			Help.BrowseURL("http://www.facebook.com");
		
		
	}

	void GUILayoutHorizontal(GameObject go, Texture2D tex){

		EditorGUILayout.BeginHorizontal();
		if (tex)
			EditorGUILayout.LabelField(new GUIContent(tex));
		go = (GameObject)EditorGUILayout.ObjectField(go, typeof(GameObject), true);
		if (go)
			tex = GetTexture2D(go.GetComponent<SpriteRenderer>().sprite);
		EditorGUILayout.EndHorizontal();
	}

	Texture2D GetTexture2D(Sprite sprite){

		if (!sprite)
			return null;

		Texture2D croppedTexture = new Texture2D( (int)sprite.rect.width, (int)sprite.rect.height );
		
		var pixels = sprite.texture.GetPixels(  (int)sprite.textureRect.x, 
		                                      (int)sprite.textureRect.y, 
		                                      (int)sprite.textureRect.width, 
		                                      (int)sprite.textureRect.height );

		croppedTexture.SetPixels( pixels );
		croppedTexture.Apply();

		return croppedTexture;
	}
}
