﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class TileSetConfig : EditorWindow {

	public Texture2D RoundTileTexture;
	public GameObject RoundTile;
	public Texture2D RoundTopTexture;
	public GameObject RoundTopTile;
	public Texture2D RoundBottomTexture;
	public GameObject RoundBottomTile;
	public Texture2D RoundRightTexture;
	public GameObject RoundRightTile;
	public Texture2D RoundLeftTexture;
	public GameObject RoundLeftTile;

	public static TileSetConfig NewConfig(){

		TileSetConfig window = (TileSetConfig)EditorWindow.GetWindow(typeof(TileSetConfig));

		return window;
	}

	public void OnGUI(){
		EditorGUILayout.LabelField("Round Tile");
		EditorGUILayout.BeginHorizontal();
		if (RoundTileTexture)
			EditorGUILayout.LabelField(new GUIContent(RoundTileTexture),GUILayout.Width(16f));
		RoundTile = (GameObject)EditorGUILayout.ObjectField(RoundTile, typeof(GameObject), true);
		if (RoundTile)
			RoundTileTexture = GetTexture2D(RoundTile.GetComponent<SpriteRenderer>().sprite);
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.LabelField("Round Top Tile");
		EditorGUILayout.BeginHorizontal();
		if (RoundTopTexture)
			EditorGUILayout.LabelField(new GUIContent(RoundTopTexture),GUILayout.Width(16f));
		RoundTopTile = (GameObject)EditorGUILayout.ObjectField(RoundTopTile, typeof(GameObject), true);
		if (RoundTopTile)
			RoundTopTexture = GetTexture2D(RoundTopTile.GetComponent<SpriteRenderer>().sprite);
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.LabelField("Round Bottom Tile");
		EditorGUILayout.BeginHorizontal();
		if (RoundBottomTexture)
			EditorGUILayout.LabelField(new GUIContent(RoundBottomTexture),GUILayout.Width(16f));
		RoundBottomTile = (GameObject)EditorGUILayout.ObjectField(RoundBottomTile, typeof(GameObject), true);
		if (RoundBottomTile)
			RoundBottomTexture = GetTexture2D(RoundBottomTile.GetComponent<SpriteRenderer>().sprite);
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.LabelField("Round Right Tile");
		EditorGUILayout.BeginHorizontal();
		if (RoundRightTexture)
			EditorGUILayout.LabelField(new GUIContent(RoundRightTexture),GUILayout.Width(16f));
		RoundRightTile = (GameObject)EditorGUILayout.ObjectField(RoundRightTile, typeof(GameObject), true);
		if (RoundRightTile)
			RoundRightTexture = GetTexture2D(RoundRightTile.GetComponent<SpriteRenderer>().sprite);
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.LabelField("Round Left Tile");
		EditorGUILayout.BeginHorizontal();
		if (RoundLeftTexture)
			EditorGUILayout.LabelField(new GUIContent(RoundLeftTexture),GUILayout.Width(16f));
		RoundLeftTile = (GameObject)EditorGUILayout.ObjectField(RoundLeftTile, typeof(GameObject), true);
		if (RoundLeftTile)
			RoundLeftTexture = GetTexture2D(RoundLeftTile.GetComponent<SpriteRenderer>().sprite);
		EditorGUILayout.EndHorizontal();

	}

	Texture2D GetTexture2D(Sprite sprite){
		
		if (!sprite)
			return null;
		
		Texture2D croppedTexture = new Texture2D( (int)sprite.rect.width, (int)sprite.rect.height );
		
		var pixels = sprite.texture.GetPixels(  (int)sprite.textureRect.x, 
		                                      (int)sprite.textureRect.y, 
		                                      (int)sprite.textureRect.width, 
		                                      (int)sprite.textureRect.height );
		
		croppedTexture.SetPixels( pixels );
		croppedTexture.Apply();
		
		return croppedTexture;
	}
}
