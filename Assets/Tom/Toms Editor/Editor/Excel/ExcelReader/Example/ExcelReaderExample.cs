﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using ExcelReader.Util;



public class ExcelReaderExample : EditorWindow {

	GUIContent m_btnCntent = new GUIContent();
	private Vector2 scrollPosition;


	[MenuItem("Window/ExcelReader/Example")]
	static void Init () {
		EditorWindow.GetWindow<ExcelReaderExample> (false, "ExcelReader");
		ExcelUtil.Instance.Init();
	}



	string excelFilePath = "";
	List<string> m_sheetNameList = new List<string>();
	ExcelSheetInfo m_excelInfo = new ExcelSheetInfo();

	
	void OnGUI () {

		if (GUILayout.Button("reopen window", GUI.skin.button, GUILayout.Width(150), GUILayout.Height(30)))
		{
			this.Close();
			Init();
		}

		ExcelUtil.Instance.GuiDragDropArea();
		excelFilePath = ExcelUtil.Instance.GuiFilePath();

		#region BeginHorizontal
		GUILayout.BeginHorizontal();

		
		m_btnCntent.text = "Read SheetNames";
		m_btnCntent.tooltip = "";
		if (GUILayout.Button(m_btnCntent, GUILayout.Height(35), GUILayout.Width(150)))
		{
			m_sheetNameList = ExcelUtil.Instance.GetSheetNameList(excelFilePath);
		}
		
		GUILayout.EndHorizontal();
		#endregion BeginHorizontal



		foreach (string sheetName in m_sheetNameList)
		{
			GUILayout.BeginHorizontal();
			GUILayout.TextField(sheetName);

			m_btnCntent.text = "Read sheet";
			m_btnCntent.tooltip = "";
			if (GUILayout.Button(m_btnCntent,  GUILayout.Width(150)))
			{
				m_excelInfo = ExcelUtil.Instance.Read(excelFilePath, sheetName);
			}
			GUILayout.EndHorizontal();
		}


		GUILayout.BeginHorizontal();
		foreach (string title in m_excelInfo.titleList.Keys)
		{
			GUILayout.TextField(title, GUILayout.Width(150));
		}
		GUILayout.EndHorizontal();


		foreach (Dictionary<string, string> rowData in m_excelInfo.data)
		{
			GUILayout.BeginHorizontal();
			foreach (string title in m_excelInfo.titleList.Keys)
			{
				GUILayout.TextField(rowData[title], GUILayout.Width(150));
			}
			GUILayout.EndHorizontal();
		}


	}





}

