﻿using UnityEngine;
using System.Collections;
using UnityEditor;
public class PrefabFinder : MonoBehaviour {

	// Creates a prefab from the selected GameObjects.
	// Creates a prefab from the selected GameObjects.
	// if the prefab already exists it asks if you want to replace it


	[MenuItem("Toms Editor/Prefab/Connect To Last Prefab")]
	static void ConnectPrefab()
	{
		GameObject[] objs = Selection.gameObjects;
		foreach (GameObject go in objs)
		{
			PrefabUtility.ReconnectToLastPrefab (go);
		}
	}
	[MenuItem("Toms Editor/Prefab/Create Prefab From Selected")]
	static void CreatePrefab() {
		GameObject[] objs = Selection.gameObjects;
		
		foreach (GameObject go in objs) {
			string localPath = "Assets/" + go.name + ".prefab";
			if (AssetDatabase.LoadAssetAtPath(localPath,typeof( GameObject))) {
				if (EditorUtility.DisplayDialog("Are you sure?", 
				                                "The prefab already exists. Do you want to overwrite it?", 
				                                "Yes", 
				                                "No"))
					CreateNew(go, localPath);
			}
			else
				CreateNew(go, localPath);
		}
	}
	[MenuItem("Examples/Create Prefab From Selected", true)]
	static bool ValidateCreatePrefab() {
		return Selection.activeGameObject != null;
	}
	
	static void CreateNew(GameObject obj, string localPath) {
		Object prefab = PrefabUtility.CreateEmptyPrefab(localPath);
		PrefabUtility.ReplacePrefab(obj, prefab, ReplacePrefabOptions.ConnectToPrefab);
	}

}
