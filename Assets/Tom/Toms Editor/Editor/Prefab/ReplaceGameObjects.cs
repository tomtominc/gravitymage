using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
public class ReplaceGameObjects : ScriptableWizard
{ 
	public bool copyValues = true;
    public GameObject NewType;
	public GameObject[] OldObjects;

	[MenuItem ("Toms Editor/Replace",false,1)]
	
	
	static void CreateWizard ()
	{
		var replaceGameObjects = ScriptableWizard.DisplayWizard <ReplaceGameObjects>("Replace GameObjects", "Replace");
		replaceGameObjects.OldObjects = Selection.gameObjects;
		

	}
	
	void OnWizardCreate ()
	{

		foreach (GameObject t in OldObjects)
		{
			Transform p = t.transform.parent;
			string n = t.name;

			GameObject newObject;
			newObject = (GameObject)PrefabUtility.InstantiatePrefab(NewType);
			newObject.transform.position = t.transform.position;
			newObject.transform.rotation = t.transform.rotation;
			newObject.transform.parent = p;
			newObject.name = n;


			DestroyImmediate(t.gameObject);
			
		}
		
	}
} 
