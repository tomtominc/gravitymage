﻿using UnityEngine;
using System.Collections;
using Tomtools.Utils;
using Tomtools.Utils.Gameplay;

public abstract class TomMono : MonoBehaviour {

    /// <summary>
    /// Shoots a ray from "_vFromLocation" in the direction of "_vRayDirection"
    /// only shoots at the distance of OPTIONAL "_fDistance" and if it doesn't hit anything
    /// you can still get a Vector3 "_vRayDirection" * 1000 if OPTIONAL "_bReturnsIfNothingHits" = true
    /// otherwise if it doesn't hit anything it will return Vector3.zero.
    /// </summary>
    /// <returns>The shoot ray.</returns>
    /// <param name="_vFromLocation">V from location.</param>
    /// <param name="_vRayDirection">V ray direction.</param>
    /// <param name="_fDistance">F distance.</param>
    /// <param name="_bReturnsIfNothingHits">If set to <c>true</c> b returns if nothing hits.</param>
    public virtual Vector3 Proc_ShootRay(Vector3 _vFromLocation,Vector3 _vRayDirection, 
        float _fDistance = Mathf.Infinity, bool _bReturnsIfNothingHits = true){

        return RayCaster.Instance.ShootRay(_vFromLocation, _vRayDirection, 
            _fDistance, _bReturnsIfNothingHits);
    }
    /// <summary>
    /// This handy function instantiates a GameObject at OPTIONAL "_vPosition"(Vector3.zero if
    /// not included) and sets its rotation to OPTIONAL "_qQuaternion" (Quaternion.Identity if 
    /// not included) if its not in the list of gameObjects already instatiated if it is
    /// then it will look and see if it's inactive and just set it to active.
    /// returns instatiated or set active gameObject
    /// and 
    /// </summary>
    /// <returns>The instantiate.</returns>
    /// <param name="_go">Go.</param>
    /// <param name="_vPosition">V position.</param>
    /// <param name="_qQuaternion">Q quaternion.</param>
    public virtual GameObject Pool_Instantiate(GameObject _go, Vector3 _vPosition = default(Vector3),
        Quaternion _qQuaternion = default(Quaternion)){

        return  Pool.Instance.InstantiateGO(_go,_vPosition,_qQuaternion);
    }
    /// <summary>
    /// Sets the gameObject to an inactive state to be used later
    /// OPTIONAL time to set inactive (if not set it gets set inactive immediately)
    /// </summary>
    /// <param name="_go">Go.</param>
    /// <param name="_fSeconds">F seconds.</param>
    public virtual void Pool_Destroy(GameObject _go, float _fSeconds = 0f){

        Pool.Instance.DestroyGO(_go, _fSeconds);
    }

    public bool IsEven(int value)
    {
        return value % 2 == 0;
    }
        
}
