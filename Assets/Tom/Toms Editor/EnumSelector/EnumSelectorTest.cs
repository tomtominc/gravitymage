﻿using UnityEngine;
using System.Collections;

public class EnumSelectorTest : MonoBehaviour {

	public enum testenum{

		zero = 0,
		one = 1,
		two = 2
	}

	[EnumSelectorAttribute]
	public testenum test;
	[SelectorAttribute(1,2)]
	public int n;

	[SelectorAttribute(2)]
	public string s = "ricky";

	[SelectorAttribute(0,1)]
	public float length = 1.0f;

	public int shitty = 5;

	public enum othertestenum{

		right = 1,
		left = 0,
		up = 2
	}

	[EnumSelectorAttribute]
	public othertestenum othertest;
	
	[SelectorAttribute(0)]
	public int othern;
	
	[SelectorAttribute(3)]
	public string others = "bobby";
	
	[SelectorAttribute(0,1)]
	public float otherlength = 1.0f;
}
