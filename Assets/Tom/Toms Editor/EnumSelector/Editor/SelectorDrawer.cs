﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using UnityEditor.AnimatedValues;
[CustomPropertyDrawer(typeof(SelectorAttribute))]
public class SelectorDrawer : PropertyDrawer {
	
	public override void OnGUI(Rect position, SerializedProperty property,GUIContent label){


		SelectorAttribute selector = attribute as SelectorAttribute;
		int currentIndex = EnumSelectorDrawer.ValueIndex;


		EditorGUI.indentLevel++;
		if (isShown(selector,currentIndex)){
			CreateField(property.GetType(),position,property,label);
		}


	}

	private bool isShown(SelectorAttribute _selector, int _currentIndex){

		foreach (int n in _selector.choices){
			if (n == _currentIndex)
				return true;
		}

		return false;

	}

	private void CreateField(Type t, Rect position, SerializedProperty property,GUIContent label){


		if (property.propertyType == SerializedPropertyType.Integer){
			EditorGUI.IntField(position,label,(int)property.intValue);
		}
		else if (property.propertyType == SerializedPropertyType.String){
			EditorGUI.TextField(position,label,(string)property.stringValue);

		}
		else if (property.propertyType == SerializedPropertyType.Float){
			EditorGUI.FloatField(position,label,(float)property.floatValue);
		}
		else if (property.propertyType == SerializedPropertyType.Vector3)
		{
			EditorGUI.Vector3Field(position,label,(Vector3)property.vector3Value);
		}
	}
}
