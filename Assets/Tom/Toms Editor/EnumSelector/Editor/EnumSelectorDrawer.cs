﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(EnumSelectorAttribute))]
public class EnumSelectorDrawer : PropertyDrawer {
	
	private static int valueIndex;

	public static int ValueIndex{
		get{return valueIndex; }
	}

	public override void OnGUI(Rect position, SerializedProperty property,GUIContent label){

		EditorGUI.PropertyField(
			position,
			property, GUIContent.none);
		valueIndex = property.enumValueIndex;
	}
}
