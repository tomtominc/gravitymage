﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class SelectorAttribute : PropertyAttribute {

	public List<int> choices 
		= new List<int>();

	public SelectorAttribute(params int[] choice){

		for(int i = 0; i < choice.Length; i++){

			choices.Add(choice[i]);
		}
	}
}
