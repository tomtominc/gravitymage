﻿using UnityEngine;
using UnityEditor;
using UnityEngine.Events;
using System.Collections;
using UnityEditor.AnimatedValues;

//[CustomEditor(typeof(GameControls))]
public class GameControlsEditor : Editor {

	GameControls myTarget;
	GameControls.InputType inputType;
	GameControls.InputType lastInputType;
	AnimBool touchesBool;
	
	AnimBool ontouchpressedbool;
	AnimBool ontouchheldbool;
	AnimBool ontouchreleasedbool;
	public UnityEvent onTouchPressed;
	public UnityEvent onTouchHeld;
	public UnityEvent onTouchReleased;

	public float buttonSize = 80f;
	public float toggledWidth = 10f;
	public float TitleWidth = 250f;

	SerializedProperty
		number;

	void OnEnable(){
		myTarget = (GameControls)target;
		inputType = myTarget.inputType;

		number = serializedObject.FindProperty("n");

		if (inputType == GameControls.InputType.Touches)
			touchesBool = new AnimBool(true);
		else
			touchesBool = new AnimBool(false);
		touchesBool.valueChanged.AddListener(Repaint);
	


		ontouchreleasedbool = new AnimBool(false);
		ontouchreleasedbool.valueChanged.AddListener(Repaint);
		ontouchpressedbool = new AnimBool(false);
		ontouchpressedbool.valueChanged.AddListener(Repaint);
		ontouchheldbool = new AnimBool(false);
		ontouchheldbool.valueChanged.AddListener(Repaint);


		GameObject go = GameObject.Find("[InputManager]");
		if (go != null){

			GameControls gameControl = go.GetComponent<GameControls>();
			if (gameControl == null)
				go.AddComponent(typeof(GameControls));

		}
		else{

			GameObject newGo = new GameObject();
			newGo.name = "[InputManager]";
			newGo.transform.position = Vector3.zero;
			newGo.AddComponent(typeof(GameControls));
		}

		Repaint ();
	}

	public override void OnInspectorGUI(){

		myTarget = (GameControls)target;
		inputType = myTarget.inputType;



		serializedObject.Update ();

		EditorGUILayout.IntSlider (number, 0, 10, new GUIContent("n") );
		serializedObject.ApplyModifiedProperties ();
		//DrawDefaultInspector();
		//inputType = (GameControls.InputType)EditorGUILayout.EnumPopup("Input",inputType);
		if (inputType != lastInputType){
			SwitchInputType();
		}
		lastInputType = inputType;
		TouchesInputGUI();
	}

	public void SwitchInputType(){
		
		switch (inputType){
			
		case GameControls.InputType.None:{
			touchesBool.target = false;
			ontouchheldbool.target = false;
			ontouchpressedbool.target = false;
			ontouchreleasedbool.target = false;
		}
			break;
		case GameControls.InputType.Touches:{
			touchesBool.target = true;
		}
			break;
		default: break;
		}
	}




	public void TouchesInputGUI(){
		if (EditorGUILayout.BeginFadeGroup(touchesBool.faded)){

			Texture touchPressed = Resources.Load ("Textures/Gestures/OnTouchPressed") as Texture;
			Texture touchHeld = Resources.Load ("Textures/Gestures/OnTouchHeld") as Texture;
			Texture touchReleased = Resources.Load ("Textures/Gestures/OnTouchReleased") as Texture;
			if (touchPressed != null){
				EditorGUILayout.Space();
				EditorGUILayout.Space();
				EditorGUILayout.Space();
				EditorGUILayout.Space();
				EditorGUILayout.BeginHorizontal();
				ontouchpressedbool.target = GUILayout.Toggle(ontouchpressedbool.target,"",GUILayout.Width(toggledWidth));
				GUIContent content = new GUIContent(touchPressed);
				EditorGUILayout.LabelField(content,GUILayout.Width(buttonSize),GUILayout.Height(buttonSize));



				ontouchheldbool.target = GUILayout.Toggle(ontouchheldbool.target,"",GUILayout.Width(toggledWidth));
				content = new GUIContent(touchHeld);
				EditorGUILayout.LabelField(content,GUILayout.Width(buttonSize),GUILayout.Height(buttonSize));

			

				ontouchreleasedbool.target = GUILayout.Toggle(ontouchreleasedbool.target,"",GUILayout.Width(toggledWidth));
				content = new GUIContent(touchReleased);
				EditorGUILayout.LabelField(content,GUILayout.Width(buttonSize),GUILayout.Height(buttonSize));



				EditorGUILayout.EndHorizontal();





			}
			else{

				Debug.Log("no texture by that name please change name of texture or put texture in the correct place");
			}
		}
		EditorGUILayout.EndFadeGroup();

		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		//EditorGUILayou (new Rect(10f,100f,1000f,100f));
		//EditorGUILayout.BeginVertical();
		EditorGUI.indentLevel++;
		EditorGUI.indentLevel++;
		EditorGUI.indentLevel++;
		EditorGUI.indentLevel++;


		if (EditorGUILayout.BeginFadeGroup(ontouchpressedbool.faded)){
			
			EditorGUILayout.BeginVertical("box",GUILayout.Width(TitleWidth));
			GUILayout.Box("Touch Pressed Events",GUILayout.Width(TitleWidth));

			EditorGUILayout.EndVertical();
		}

		EditorGUILayout.EndFadeGroup();
		//EditorGUI.indentLevel--;
		//EditorGUILayout.EndVertical();
		//GUILayout.EndArea();

	}
}
