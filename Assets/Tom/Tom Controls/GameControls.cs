﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;
using System.Collections.Generic;
public class GameControls : TomMono {

	public enum InputType{

		None,
		Touches,
		Controller,
		Keyboard,
		ControllerAndKeyboard
	}

	public InputType inputType;
	public UnityEvent OnTouchPressedEvent;
	public UnityEvent OnTouchHeldEvent;
	public UnityEvent OnTouchReleasedEvent;


	private static Dictionary<int,List<Action>> KeyboardKeyDownmapping
		= new Dictionary<int, List<Action>>();
	private static Dictionary<int,List<Action>> KeyboardKeyHeldmapping
		= new Dictionary<int, List<Action>>();
	private static Dictionary<int,List<Action>> KeyboardKeyUpmapping
		= new Dictionary<int, List<Action>>();
	private static List<KeyCode> KeysToDetectForUp
		= new List<KeyCode>();

	#region Touch Controls
	public delegate void TouchPressed();
	public static event TouchPressed eTouchPressed;
	public static void OnTouchPressed(){
		if (Input.GetButtonDown("Fire1")){
			if (eTouchPressed != null){
				eTouchPressed();
			}
		}

	
	}

	public delegate void TouchHeld();
	public static event TouchHeld eTouchHeld;
	public static void OnTouchHeld(){
		if (Input.GetButton("Fire1")){
			if (eTouchHeld != null){
				eTouchHeld();
			}
		}
	}

	public delegate void TouchRelease();
	public static event TouchRelease eTouchRelease;
	public static void OnTouchRelease(){
		if (Input.GetButtonUp("Fire1")){
			if (eTouchRelease != null){
				eTouchRelease();
			}
		}
	}

	#endregion

	#region Keyboard Controls

	public static void RegisterKeyActionKeyDown(KeyCode k, Action action){
		if (KeyboardKeyDownmapping.ContainsKey((int)k)){
			KeyboardKeyDownmapping[(int)k].Add(action);
		}
		else{
			List<Action> actionList = new List<Action>();
			actionList.Add(action);
			KeyboardKeyDownmapping.Add((int)k,actionList);
		}
	}

	public static void OnKeyDown(){
		if (Input.anyKeyDown){
		
		KeyCode k = (KeyCode)System.Enum.Parse(typeof(KeyCode),Input.inputString);
			int n = (int)k;
			if (KeyboardKeyDownmapping.ContainsKey(n)){
				KeysToDetectForUp.Add(k);
				foreach (Action action in KeyboardKeyDownmapping[n]){
					action.Invoke();
				}
			}

		}
	}

	public static void RegisterKeyActionKeyHeld(KeyCode k, Action action){
		if (KeyboardKeyHeldmapping.ContainsKey((int)k)){
			KeyboardKeyHeldmapping[(int)k].Add(action);
		}
		else{
			List<Action> actionList = new List<Action>();
			actionList.Add(action);
			KeyboardKeyHeldmapping.Add((int)k,actionList);
		}
	}
	public static void OnKeyHeld(){
		if (Input.anyKey){
			
			KeyCode k = (KeyCode)System.Enum.Parse(typeof(KeyCode),Input.inputString);
			int n = (int)k;
			if (KeyboardKeyHeldmapping.ContainsKey(n)){
				foreach (Action action in KeyboardKeyHeldmapping[n]){
					action.Invoke();
				}
			}
			
		}
	}

	public static void RegisterKeyActionKeyUp(KeyCode k, Action action){
		if (KeyboardKeyUpmapping.ContainsKey((int)k)){
			KeyboardKeyUpmapping[(int)k].Add(action);
		}
		else{
			List<Action> actionList = new List<Action>();
			actionList.Add(action);
			KeyboardKeyUpmapping.Add((int)k,actionList);
		}
	}

	public static void OnKeyUp(){
		foreach (KeyCode k in KeysToDetectForUp){
			if (Input.GetKeyUp(k)){
				int n = (int)k;
				if (KeyboardKeyUpmapping.ContainsKey(n)){
					foreach (Action action in KeyboardKeyUpmapping[n]){
						action.Invoke();
					}
				}
				
			}
		}
	}

	#endregion

	private void Update(){
		switch (inputType){
		case InputType.Touches:
			OnTouchPressed();
			OnTouchHeld();
			OnTouchRelease();

			break;
		case InputType.Keyboard:
			OnKeyDown();
			OnKeyHeld();
			OnKeyUp();
			break;
		}
	}

}





































































































































