﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* TomTool Script please do not add anything from 
 * current project and make as generic as possible */

/* Phone Script - This script is fully optimized for phone use 
 * and also able to use this when playing in editor */

/* Touch the screen and the object that has this script attached will
 * move to that location at a rate of "speed" */

/* Update V1.01 : you can now select the mechanic you want to use
 * MOVETOWARD :  pushes the gameObject toward the last touch location
 * MOVEAWAY : pushes the gameObject in the opposite direction of the finger */

namespace Tomtools.Utils.Gameplay{
    [RequireComponent(typeof(Rigidbody2D))]
	public class MoveWithTouch : TomMono {

		#region Properties
		[HideInInspector]
		public BoxCollider2D objectCollider;
		/* moves to touch location at this rate */
		[Group("Properties")]
		public float speed = 3f;
		/* the script will not actually move the character if this bool is false */
		/* change this value in another script to make this script not move target inactive */
		/* this is good if you want to move the character on your own but get a touchLocation */
		[Group("Affecting Attributes")]
		public bool moveWithTouch = true;
		/* this can be turned off if you don't want to update the touchlocation until
		 * the gameobject reaches the goal or touchLocation */
		[Group("Affecting Attributes")]
		public bool upateTouchWhenMoving = true;
		/* the touch requirement is bascially is it a touch up or a touch down that makes this happen */
		public enum TouchRequirement { touchup, touchdown };
		[Group("Affecting Attributes")]
		public TouchRequirement touchRequirement;
		/* this is how the touch location affects the gameobject before 
		 * I just had the gameObject move to the touch but now i want the 
		 * system to be able to switch so you can move to , move towards, and move away */
		public enum TouchMechanic{ MoveTo, MoveTowards, MoveAway};
		/* touch mechanic */
		[Group("Affecting Attributes")]
		public TouchMechanic touchMechanic;
		[Group("Properties")]
		public float pushStrength = 2f;
		/* this cancels the action if the touch is moved around at all during the touchdown phase */
		[Group("Affecting Attributes")]
		public bool cancelIfTouchMoves = false;
		/* This is used when you have to ray cast to another location and not 
		 * the exact touch location */
		[Group("Advanced Attributes")]
		[Tooltip("This is used when you have to ray cast to " +
				 "another location and not the exact touch location")]
		public bool raycastToLocation = false;
		/* What direction do you want to raycast? */ 
		/* this option only works if you have rayCastToLocation on */
		[System.Serializable]
		public enum RaycastDirection {up,down,left,right};
		[Group("Advanced Attributes")]
		public RaycastDirection raycastDirection;
		private Vector3 rayDirection;
		/* If you have the raycastIdentifier as a tag then fill this in to see your tag */
		[Group("Advanced Attributes")]
		public string raycastTag;
		/* if you have the raycastIdentifier as a layer number then this is the layer it will
		 * raycast to to find anything */
		[Group("Advanced Attributes")]
		public LayerMask raycastLayer;
		/* if true this provides the feature of raycasting in front before actually moving
		 * this is good if you dont want to try and go through a wall 
		 * or building */
		[Group("Advanced Attributes")]
		public bool lookOutBeforeMoving;
		/* all the tags that you don't want to move to even if a touch was 
		 * called */
		/* only works if "lookOutBeforeMoving" is on */
		[Group("Advanced Attributes")]
		public List<string> tagsToNotMoveInto = new List<string>();
		/* this is used to make the distance you want to stop before actually
		 * hitting the object that you don't want to move into */
		/* only works if "lookOutBeforeMoving" is on */
		[Group("Advanced Attributes")]
		public float distanceToStop = 1f;
		/* I'll take this location in when touching */
		/* grab this value by using the TouchLocation() method */
		private Vector3 touchLocation;
		/* this is true when the target is moving to the location */
		/* able to check this with IsMoving() to see if your gameObject is moving or not */
		/* readonly property you may not set this */
		private bool isMoving = false;
		/* this shows you the distance between the location of the touch and the current gameObject */
		/* this is good for slowing down or activating something important */
		/* get this value by using the DistanceToTouch() method */
		private float distanceToTouch;
		/* this has to be false in order to call a touchup event */ 
		/* this is so the action can be canceled if the mouse/finger was moved anywhere
		 * during the down phase of the call */
		private bool actionCanceled = false;


		[Group("Affecting Atributes")]
		public bool cancelOnSomeTags;
		[Group("Affecting Atributes")]
		public List<string> tagsToCancelTouch
			= new List<string>();

        public bool isEnabled = true;
		

		#endregion 

		#region Getters
		/* get if the current gameObject is being moved by this script */
		public bool IsMoving(){return isMoving;}
		/* get the touchLocation of this touch */ 
		/* this is good when you dont want to move to exactly the touch location but just get 
		 * one of the x, y, or z values */
		/* this can be good for raycasting and other things */
		public Vector3 TouchLocation(){return touchLocation;}
		/* this value will be updated even if this script is not directly moving the gameObject */
		public float DistanceToTouch(){ return distanceToTouch;}
		#endregion

		#region Key Functions to be called by other scripts
		public void StopMoving(){ isMoving = false;}
		#endregion

		public void Awake(){
			objectCollider = GetComponent<BoxCollider2D>();
			switch (raycastDirection){
				case RaycastDirection.up:
					rayDirection = Vector3.up;
					break;
				case RaycastDirection.down:
					rayDirection = Vector3.down;
					break;
				case RaycastDirection.right:
					rayDirection = Vector3.right;
					break;
				case RaycastDirection.left:
					rayDirection = Vector3.left;
					break;
			}


			
		}

		public void MoveUpdate(){
            if (isEnabled)
            {
                if (upateTouchWhenMoving)
                {
                    UpdateTouchLocation();
                }
                else if (!isMoving)
                {
                    UpdateTouchLocation();

                }

                if (isMoving && moveWithTouch && !actionCanceled)
                {
                    SwitchMechanics();
                }
                else
                {
                    UpdateDistanceToTouch();
                }
            }
		}

		/// <summary>
		/// Updates the touch location.
		/// this will get the touch location if they 
		/// press down on the phone or use the mouse in the editor
		/// </summary>
		public void UpdateTouchLocation(){


    		if (touchRequirement == TouchRequirement.touchup){

    			TouchPhaseCheck();
    			UpdateTouchLocationOnUp();
    		}
    		else{
    			UpdateTouchLocationOnDown();
    		}

		
		}


		public void SwitchMechanics(){

			switch (touchMechanic){
			case TouchMechanic.MoveTo:MoveToFinger();break;
			case TouchMechanic.MoveTowards:MoveTowardFinger();break;
			case TouchMechanic.MoveAway:MoveAwayFinger(); break;
			}
		}
		/// <summary>
		/// Moves the game object.
		/// This is the actual code that moves the gameObject to the touchLocation
		/// </summary>
		public void MoveToFinger(){

			if (touchLocation != Vector3.zero){
				RaycastOutFront(touchLocation);
				float step = speed * Time.deltaTime;
				transform.position = Vector3.MoveTowards(transform.position,touchLocation,step);
				distanceToTouch = Vector3.Distance(transform.position,touchLocation);

				if (distanceToTouch < 0.0001f){

					distanceToTouch = 0;
					isMoving = false;
				}

			}

		}

		/// <summary>
		/// Move toward finger uses the rigid body and pushes the 
		/// object toward the fingers last position. 
		/// this is great for "bumping" the player to your finger
		/// </summary>
		public void MoveTowardFinger(){
			if (touchLocation != Vector3.zero){
				RaycastOutFront(touchLocation);
				Vector3 forceAmt = (transform.position - touchLocation).normalized;
				Vector2 forceVector = forceAmt * pushStrength;
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				GetComponent<Rigidbody2D>().AddForce(-forceVector);
				isMoving = false;
			}

		}

		public void MoveAwayFinger(){
			if (touchLocation != Vector3.zero){
				RaycastOutFront(touchLocation);
				Vector3 forceAmt = (transform.position - touchLocation).normalized;
                Debug.Log("Force vector being applyed ");
				Vector2 forceVector = forceAmt * pushStrength;
				GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				GetComponent<Rigidbody2D>().AddForce(forceVector);
				isMoving = false;
			}
		}

		/// <summary>
		/// Updates the distance to touch.
		/// This method is only called if the method is not being moved by this script
		/// It updates the distance from the touch still to give information
		/// about the distance
		/// </summary>
		public void UpdateDistanceToTouch(){
			distanceToTouch = Vector3.Distance(transform.position,touchLocation);
		}


		#region Helpers
		private void RaycastOutFront(Vector3 mousePos){
			if (lookOutBeforeMoving){
				RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position,(new Vector3(mousePos.x,transform.position.y) - transform.position).normalized);
				foreach (RaycastHit2D hit in hits){
					foreach (string tag in tagsToNotMoveInto){
						if (hit.collider.tag == tag){
							
							float distanceToNotMoveTarget = Vector3.Distance(hit.collider.transform.position,
							                                                 transform.position);
							if (distanceToNotMoveTarget <= distanceToStop){
								if (isMoving)
									isMoving = false;
								else
									actionCanceled = true;
							}
						}
					}
				}
			}
		}

		private void RaycastToCancel(Vector3 mousePos){

			RaycastHit2D[] hits = Physics2D.RaycastAll(mousePos,Vector3.forward);
			foreach (RaycastHit2D hit in hits){
				if (hit.collider){
					foreach (string t in tagsToCancelTouch){
						if (t == hit.collider.tag){
							return;
						}
					}
				}
			}

			touchLocation = mousePos;

		}
		private void TouchPhaseCheck(){

			if (Application.isEditor){
				if (cancelIfTouchMoves){
					if (Input.GetButton("Fire1")){
						Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
						print ("mouse position = " + mousePos);

					
						if(Input.GetAxis("Mouse X")<0){
							actionCanceled = true;
						}
						if(Input.GetAxis("Mouse X")>0){
							actionCanceled = true;
						}

						RaycastOutFront(mousePos);

					}
				}
			}


		}

		/// <summary>
		/// Called in the UpdateTouchLocation method to break up how long it is
		/// </summary>
		private void UpdateTouchLocationOnDown(){
			
			if (Input.GetButtonDown("Fire1")){
				
				Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				if (raycastToLocation){
					
					RayCastHelper(mousePos);
				}
				else
					touchLocation = new Vector3(mousePos.x,mousePos.y,0f);
				if (moveWithTouch)
					isMoving = true;
			}
		}
		/// <summary>
		/// Called in the UpdateTouchLocation method to break up how long it is
		/// </summary>
		private void UpdateTouchLocationOnUp(){

			if (Input.GetButtonUp("Fire1")){

				if (!actionCanceled){
					Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					if (raycastToLocation){
						
						RayCastHelper(mousePos);
					}
					else
						RaycastToCancel(new Vector3(mousePos.x,mousePos.y,0f));

					if (moveWithTouch)
						isMoving = true;
				}
				else 
					actionCanceled = false;
			}

		}
		/// <summary>
		/// Called In the UpdateTouchLocation method
		/// to break up how long it is
		/// </summary>
		/// <param name="mousePos">Mouse position.</param>
		private void RayCastHelper(Vector3 mousePos){
		
			/*int uiLayerMask = 1 << 5;
			int layerMask = 1 << raycastLayer;
			int combinedLayer = uiLayerMask | layerMask;*/
			RaycastHit2D hit = Physics2D.Raycast(mousePos, rayDirection,Mathf.Infinity,raycastLayer);
			if (hit.collider != null && hit.collider.tag == raycastTag){

				Vector3 hitPos = hit.collider.transform.position;
				float ySize = hit.collider.bounds.size.y/2;
				float pointY = ySize + (objectCollider.size.y/2) + hitPos.y;

				touchLocation = new Vector3 (hit.point.x, pointY);
				
			}
			
		
		}

		
		#endregion 

	}
}
























































