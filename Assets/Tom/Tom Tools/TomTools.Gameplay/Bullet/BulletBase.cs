#region Class Information
/*-------------------------------------------------
 File:           		BulletBase .cs
 Project:          		Wizzam Platformer
 Solution:           	Wizzam
 Description:        	The base class for alll bullets


    Name            Date        Version Description                    
---------------------------------------------------
Thomas Ferrer               01/22/2015 16:36:48             1.0
---------------------------------------------------*/
# endregion


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletBase : TomMono {



	[Group("Properties")]
	public float maxVelocity = 10f;
	[Group("Properties")]
	public bool startAwake = false;
	[Group("Properties")]
	public bool useGravity = true;
	[Group("Properties")]
	public float useGravityAfterTime = 1f;
	[Group("Properties")]
	public bool isLookAt = true;
	[Group("Properties")]
	public bool destroyAfterSeconds = true;
	[Group("Properties")]
	public float secondsToDestroy = 5f;
	[Group("Properties")]
	public LayerMask collisionLayer;
	[Group("Properties")]
	public bool usePooling = false;


	[Group("Physics")]
	public Vector3 gravity = new Vector3(0f,-9.8f,0f);
	[Group("Physics")]
	public float mass = 2f;
	[Group("Physics")]
	public float friction = 0.05f;

	[Group ("Move Demensions")]
	public bool x = true;
	[Group ("Move Demensions")]
	public bool y = true;
	[Group ("Move Demensions")]
	public bool z = true;
	
	[System.Serializable]
	public class Explosions{

		public GameObject explosionParticle;
		public float explodeLifespan;
	}
	[Group("Explode")]
	public bool doesExplode = true;
	[Group("Explode")] //particle to instatiate on explode
	public List<Explosions> explosions;



	public delegate void Collide(Transform t);
	public event Collide eCollide;
	public void OnCollide(Transform t){

		if (eCollide != null){
			print (t);
			eCollide(t);
		}
	}

	public delegate void Explode();
	public event Explode eExplode;
	public void OnExplode(){
		if (eExplode != null){
			eExplode();
		}
	}

	#region Private Properties
	/* intial direction to move set by MoveInDirection */
	public Vector3 DirectionToMove;
	/* velocity of object */
	private Vector3 velocity;
	/* this is the gravity vector gets its value from the public vector */
	private Vector3 internalGravity;
	#endregion

	void Awake(){

		if (!startAwake)
			gameObject.SetActive(false);
		else if (destroyAfterSeconds){
			if (usePooling)
				Pool_Destroy(gameObject,secondsToDestroy);
			else
				Destroy(gameObject,secondsToDestroy);
		}

		if (gameObject.activeSelf)
			StartCoroutine("UseGravityAfterTime");



		eCollide += CollideWithObject;
		eExplode += InitExplosions;
	}

	void Start () {
		
	}
	

	void Update () {

		Move ();
	}

	#region Update
	public void Move(){

		if (gameObject.activeSelf){


		}
		else{

			print ("use the MoveInDirection(Vector3) function to start the bullet");
		}
	}
	#endregion
	#region Init
	public void MoveInDirection(Vector3 _direction){

		if (!gameObject.activeSelf){
			gameObject.SetActive(true);
			StartCoroutine("UseGravityAfterTime");
		}
		if (destroyAfterSeconds){
			if (usePooling)
				Pool_Destroy(gameObject,secondsToDestroy);
			else
				Destroy(gameObject,secondsToDestroy);

		}

		GetComponent<Rigidbody>().AddForce(_direction * maxVelocity,ForceMode.VelocityChange);

	}
	#endregion

	#region Coroutines
	public IEnumerator UseGravityAfterTime(){
		yield return new WaitForSeconds(useGravityAfterTime);
		GetComponent<Rigidbody>().useGravity = useGravity;
	}
	#endregion

	#region Explode
	void InitExplosions(){

		foreach (Explosions e in explosions){
			if (usePooling){
				GameObject go = Pool_Instantiate(e.explosionParticle,transform.position,Quaternion.identity);
				Pool_Destroy(go,e.explodeLifespan);
			}
			else{
				GameObject go = Instantiate(e.explosionParticle,transform.position,Quaternion.identity) as GameObject;
				Destroy(go,e.explodeLifespan);
			}
		}

	}
	#endregion
	
	#region Collision Functions
	void OnTriggerEnter(Collider other){

		if (other.transform){
			OnCollide (other.transform);
			if (doesExplode){
				OnExplode();
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other){

		if (other.transform){
			OnCollide (other.transform);
		}
	}

	void OnCollisionEnter(Collision other){

		if (other.transform){
			OnCollide (other.transform);
		}
	}

	void OnCollisionEnter2D(Collision2D other){

		if (other.transform){
			OnCollide (other.transform);
		}
	}

	void CollideWithObject(Transform t){

		if((collisionLayer.value & 1<<t.gameObject.layer) == 1<<t.gameObject.layer){
			Pool_Destroy(gameObject);
		}
	}

	#endregion
	
}














































































