﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveAtTempo : TomMono {

	//best way to do it
	public bool callFromAnimator = false;

	public bool FlipSpriteX;
	public bool FlipSpriteY;
	public float speed = 1.0f;
	public float moveAmount = 1.0f;
	public float tempo = 2.0f; // move this game object "moveAmount" every "tempo"

	private Vector3 nextMovePosition;
	private float nextMoveTemp;
	private float moveEndTemp;
	private bool isMovingForward = false;

	public List<Transform> Path = new List<Transform>();
	private int pathIndex = 0;

	private Vector3 startPosition;

	private void Awake(){

	}

	private void Manual(){

		if (isMovingForward){
			
			float step = speed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position,Path[pathIndex].position,step);
			print (pathIndex);
			float distance = Vector3.Distance(transform.position,Path[pathIndex].position);
			
			moveEndTemp += Time.deltaTime;
			
			if (moveEndTemp >= moveAmount){
				moveEndTemp = 0;
				isMovingForward = false;
				if (distance <= 0.5f){
					if (pathIndex < 1){
						pathIndex++;
					}
					else
					{
						pathIndex = 0;
					}
				}
			}
			
		}
		else{
			nextMoveTemp += Time.deltaTime;
			if (nextMoveTemp >= tempo){
				
				nextMoveTemp = 0;
				isMovingForward = true;
				
			}
		}
	}

	private void FlipSprite(){
		Vector3 heading = transform.position - Path[pathIndex].position;
		if (FlipSpriteX){
			if (heading.x < -0.1f){ //Im moving right
				transform.localScale = new Vector3(1,transform.localScale.y,transform.localScale.z);
				
			}
			else{ // Im moving left
				transform.localScale = new Vector3(-1,transform.localScale.y,transform.localScale.z);
			}
		}
		
		if (FlipSpriteY){
			if (heading.y < -0.1f){ //Im moving up
				transform.localScale = new Vector3(transform.localScale.x,1,transform.localScale.z);
				
			}
			else{ // Im moving down
				transform.localScale = new Vector3(transform.localScale.y,-1,transform.localScale.z);
			}
		}
	}

	private void MoveForward(){

		FlipSprite();
		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position,Path[pathIndex].position,step);

	}

	private void EndMoveForward(){

		float distance = Vector3.Distance(transform.position,Path[pathIndex].position);
		if (distance <= 0.5f){
			if (pathIndex < 1){
				pathIndex++;
			}
			else
			{
				pathIndex = 0;
			}
		}

		FlipSprite();
	}
	private void Update(){
		if (!callFromAnimator)
			Manual();
	}
}
