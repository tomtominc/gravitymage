﻿using System;
using UnityEngine;

namespace Tomtools.Utils.Steer2D
{
    public class Seek : SteeringBehaviour
    {
        public Transform Target;

        public override Vector2 GetVelocity()
        {
            return (((Vector2)Target.position - (Vector2)transform.position).normalized * agent.MaxVelocity) - agent.CurrentVelocity;   
        }
    }
}
