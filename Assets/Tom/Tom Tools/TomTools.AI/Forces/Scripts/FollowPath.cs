﻿using UnityEngine;
using System.Collections.Generic;
namespace Tomtools.Utils.Steer2D
{
    public class FollowPath : SteeringBehaviour
    {
		public bool FlipSpriteX = false;
		public bool FlipSpriteY = false;
		public delegate void FollowingPath();
		public event FollowingPath eFollowingPath;
		public void OnFollowingPath(){
			if (eFollowingPath != null){
				eFollowingPath();
			}
		}
		public delegate void EndPath();
		public event EndPath eEndPath;
		public void OnEndPath(){
			if (eEndPath != null){
				eEndPath();
			}
		}
		public List<Transform> Path = new List<Transform>();
        public float SlowRadius = 1;
        public float StopRadius = 0.2f;
        public float NextCoordRadius = 0.2f;
        public bool Loop = false;

        public bool DrawGizmos = false;


        public bool Finished
        {
            get
            {
                return currentPoint >= Path.Count;
            }
        }

		[HideInInspector]
        public int currentPoint = 0;

		public void SetNewPath(List<Transform> path)
        {
            Path = path;
            currentPoint = 0;
        }

        public override Vector2 GetVelocity()
        {
            Vector2 velocity;


			if (currentPoint >= Path.Count)
                return Vector2.zero;
			else if (!Loop && currentPoint == Path.Count - 1)
				velocity = arrive(Path[currentPoint].position);
            else
				velocity = seek(Path[currentPoint].position);

			float distance = Vector3.Distance(transform.position, Path[currentPoint].position);

			if ((currentPoint == Path.Count - 1 && distance < StopRadius) || distance < NextCoordRadius)
            {
                currentPoint++;
                if (Loop && currentPoint == Path.Count)
                    currentPoint = 0;


            }


			if ((FlipSpriteX || FlipSpriteY) && currentPoint < Path.Count){

				Vector3 heading = transform.position - Path[currentPoint].position;
				if (FlipSpriteX){
					if (heading.x < -0.1f){ //Im moving right
						transform.localScale = new Vector3(1,transform.localScale.y,transform.localScale.z);

					}
					else{ // Im moving left
						transform.localScale = new Vector3(-1,transform.localScale.y,transform.localScale.z);
					}
				}

				if (FlipSpriteY){
					if (heading.y < -0.1f){ //Im moving up
						transform.localScale = new Vector3(transform.localScale.x,1,transform.localScale.z);
						
					}
					else{ // Im moving down
						transform.localScale = new Vector3(transform.localScale.y,-1,transform.localScale.z);
					}
				}

			}


            return velocity;
        }

        Vector2 seek(Vector2 targetPoint)
        {
            return ((targetPoint - (Vector2)transform.position).normalized * agent.MaxVelocity) - agent.CurrentVelocity;   
        }

        Vector2 arrive(Vector2 targetPoint)
        {
            float distance = Vector3.Distance(transform.position, (Vector3)targetPoint);
            Vector2 desiredVelocity = (targetPoint - (Vector2)transform.position).normalized;

            if (distance < StopRadius)
                desiredVelocity = Vector3.zero;
            else if (distance < SlowRadius)
                desiredVelocity = desiredVelocity * agent.MaxVelocity * ((distance - StopRadius) / (SlowRadius - StopRadius));
            else
                desiredVelocity = desiredVelocity * agent.MaxVelocity;

            return desiredVelocity - agent.CurrentVelocity;
        }

        void OnDrawGizmos()
        {
            if (DrawGizmos)
            {
                if (currentPoint < Path.Count)
                {
                    Gizmos.color = Color.red;
					Gizmos.DrawSphere(Path[currentPoint].position, .05f);

                    if (currentPoint == Path.Count - 1)
                    {
                        Gizmos.color = Color.blue;
						Gizmos.DrawWireSphere(Path[currentPoint].position, SlowRadius);

                        Gizmos.color = Color.red;
						Gizmos.DrawWireSphere(Path[currentPoint].position, StopRadius);
                    }
                    else
                    {
                        Gizmos.color = Color.blue;
						Gizmos.DrawWireSphere(Path[currentPoint].position, NextCoordRadius);
                    }
                }

                Gizmos.color = Color.magenta;
				Gizmos.DrawLine(transform.position, Path[0].position);
				for (int i = 0; i < Path.Count - 1; ++i)
                {
					Gizmos.DrawLine(Path[i].position, Path[i + 1].position);
                }
            }
        }
    }
}