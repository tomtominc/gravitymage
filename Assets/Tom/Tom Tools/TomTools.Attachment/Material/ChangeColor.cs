﻿using UnityEngine;
using System.Collections;

public class ChangeColor : TomMono {

	public Color minColor;
	public Color maxColor;

	private Renderer myRenderer;
	// Use this for initialization
	void Start () {
		myRenderer = GetComponent<Renderer>();

	}

	public void Percent(float _percent){

		Vector3 A = new Vector3(minColor.r,minColor.g,minColor.b);
		Vector3 B = new Vector3(maxColor.r,maxColor.g, maxColor.b);
		Vector3 newColor = _percent * Vector3.Normalize(B - A) + A;
		Color color = new Color(newColor.x,newColor.y,newColor.z);
		myRenderer.material.color = color;
	}
}
