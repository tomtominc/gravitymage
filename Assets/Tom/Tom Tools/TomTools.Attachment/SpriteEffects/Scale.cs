﻿#region Class Information
/*-------------------------------------------------
 File:           		Scale.cs
 Project:          		$projectname$
 Solution:           	$solution$
 Description:        	$description$
                
---------------------------------------------------
Copyright (c) 2014 Thomas Ferrer $Date$
---------------------------------------------------*/
# endregion

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Tomtools.Utils{
	public class Scale : TomMono {

		#region Public Variables
		public enum ScaleType{
			SCALETOFROM
		}
		public ScaleType scaleType;
		#endregion

		public Vector3 LargestScale;
		public Vector3 SmallestScale;
		public float scaleTime;
		public bool startScaleOnAwake = true;

		#region Private Variables
		private bool scaleUp;
		private bool startScale = false;
		private bool stopWhenLarge = false;
		#endregion

		#region Build - In Unity
		void Awake(){
			if (startScaleOnAwake)
				startScale = true;
		}
	
		void Start () {
	
		}
	
	
		void Update () {

			if (startScale){
				switch (scaleType){
				case ScaleType.SCALETOFROM:
					ScaleToFrom();
					break;

				}
			}
	
		}
		#endregion

		void ScaleToFrom(){

			if (scaleUp){
				float step = scaleTime * Time.deltaTime;

				transform.localScale = Vector3.MoveTowards(transform.localScale,LargestScale,step);
				float distance = Vector3.Distance(transform.localScale,LargestScale);
				if (distance < 0.001f){
					transform.localScale = LargestScale;
					scaleUp = false;

					if (stopWhenLarge){
						startScale = false;
					}
				}
			}
			else{
				float step = scaleTime * Time.deltaTime;
				
				transform.localScale = Vector3.MoveTowards(transform.localScale,SmallestScale,step);
				float distance = Vector3.Distance(transform.localScale,SmallestScale);
				if (distance < 0.001f){
					transform.localScale = SmallestScale;
					scaleUp = true;
				}
			}
		}



		#region Helpers 
		public void SetLargest(){
			stopWhenLarge = true;
		}
		#endregion


	}


}



















































