﻿using UnityEngine;
using System.Collections;

namespace Tomtools.Attachment{
	[RequireComponent(typeof(SpriteRenderer))]
	public class Fade : TomMono {

		public enum FadeType{
			FADEOUTDESTROY,
			FADEINANDOUT
		}
		public FadeType fadeType;

		[Group("Fade in and out")]
		[Range(0,100)]
		public float fadeInAlpha;
		[Group("Fade in and out")]
		[Range(0,100)]
		public float fadeOutAlpha;

		[Group("Universal")]
		public float timeToFade;
		[Group("Universal")]
		public bool startFadeOnStart;

		private bool startFadeOut;
		private float maxFadeTime;
		private SpriteRenderer myRenderer;
		private bool fadeOut;

		// Use this for initialization
		void Start () {
		
			maxFadeTime = timeToFade;
			myRenderer = GetComponent<SpriteRenderer>();

			if (startFadeOnStart) {

				startFadeOut = true;
			}
		}
		
		// Update is called once per frame
		void Update () {
			switch (fadeType){
			case FadeType.FADEINANDOUT:
				FadeInAndOut();
				break;
			case FadeType.FADEOUTDESTROY:
				FadeOutDestroy();
				break;
			}
		
		}

		void FadeInAndOut(){
			if (startFadeOut) {

				if (fadeOut){
					timeToFade -= Time.deltaTime;
					float percentOfTimeLeft = timeToFade / maxFadeTime;
					float alpha = Mathf.Clamp((fadeInAlpha * percentOfTimeLeft),fadeOutAlpha,fadeInAlpha);
					float newAlpha = alpha/ 100f;
					myRenderer.color = new Color(myRenderer.color.r,
					                             myRenderer.color.g,
					                             myRenderer.color.b,
					                           newAlpha);


					if (alpha <= fadeOutAlpha){
						fadeOut = false;
					}
//					if (timeToFade <= 0f){
//						timeToFade = 0f;
//						fadeOut = false;
//					}


				}
				else {

					timeToFade += Time.deltaTime;
					float percentOfTimeLeft = timeToFade / maxFadeTime;
					float alpha = Mathf.Clamp((fadeInAlpha * percentOfTimeLeft),fadeOutAlpha,fadeInAlpha);
					float newAlpha = alpha /100f;

					myRenderer.color = new Color(myRenderer.color.r,
					                             myRenderer.color.g,
					                             myRenderer.color.b,
					                             newAlpha);
					if ( alpha >= fadeInAlpha){
						fadeOut = true;
					}
//					if (timeToFade >= maxFadeTime){
//						fadeOut = true;
//						timeToFade = maxFadeTime;
//					}

				}
				
				
				
			}
		}


		void FadeOutDestroy(){
			if (startFadeOut) {
				
				timeToFade -= Time.deltaTime;
				float percentOfTimeLeft = timeToFade / maxFadeTime;
				myRenderer.color = new Color(myRenderer.color.r,
				                             myRenderer.color.g,
				                             myRenderer.color.b,
				                             percentOfTimeLeft);
				
				if (timeToFade <= 0)
					Destroy (gameObject);
				
			}
		}

	}


}


















































































