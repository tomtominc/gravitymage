using UnityEngine;
using System.Collections;

public class AutoRotate : TomMono
{
	public Vector3 rotation;
	public float speed = 1f;
	
	void Update ()
	{

		this.transform.Rotate(rotation * (Time.deltaTime * speed), Space.World);
	
	}
}
