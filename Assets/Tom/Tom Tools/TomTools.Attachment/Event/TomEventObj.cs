﻿#region Class Information
/*-------------------------------------------------
 File:           		TomEventObj.cs
 Project:          		$projectname$
 Solution:           	$solution$
 Description:        	$description$
                
---------------------------------------------------
Copyright (c) 2014 Thomas Ferrer $Date$
---------------------------------------------------*/
# endregion

using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace Tomtools.Attachment{
	public class TomEventObj : TomMono {

		#region Public Variables
		public bool eventEnabled = true;
		public string playerTag = "Player";
		public UnityEvent collideEvent;

		#endregion

		#region Private Variables

		#endregion

		#region Build - In Unity
		public virtual void Awake(){
			if (collideEvent.GetPersistentEventCount() < 1)
				collideEvent.AddListener(defaultOnCollideAction);
		}
	
		public virtual void Start () {
	
		}
	
	
		public virtual void Update () {
	
		}

		public virtual void defaultOnCollideAction(){

			print ("Called default collide action on "+  gameObject);
		}


		
		#endregion

		#region Collision
		public virtual void OnTriggerEnter2D(Collider2D other){

			if (other.tag == playerTag){
				collideEvent.Invoke();
			}
		}
		#endregion
	}
}
