﻿#region Class Information
/*-------------------------------------------------
 File:           		CameraZoom.cs
 Project:          		SpacebumpV1.0
 Solution:           	SpacebumpV1.0-csharp.sln
 Description:        	${description}$
                
---------------------------------------------------
Copyright (c) 2014 Thomas Ferrer December 17, 2014
---------------------------------------------------*/
# endregion

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[RequireComponent(typeof(Camera))]
public class CameraZoom : TomMono {

	#region Public member variables
    public float zoomInSpeed = 5.0f;
    public float zoomOutSpeed = 5.0f;
    public float minZoomLevel = 6f;
    public float maxZoomLevel = 10f;
	#endregion

	#region Public member properties

	#endregion

	#region Private member variables
    private bool bzoomIn = false;
    private bool bzoomOut = false;
	#endregion
	
	#region Private member properties

	#endregion

	#region Unity Built-in Methods

	void Awake(){
     
	}
        
	void Start(){
       
	}
        
	void Update(){
     
      
       UpdateZoomCam();
        
	}

    void UpdateZoomCam(){
        if (bzoomIn)
        {

            ZoomIn();
        }
        if (bzoomOut)
        {
            ZoomOut();

        }
    }

	#endregion

    #region Helpers 
    void ZoomOut(){
        GetComponent<Camera>().orthographicSize += zoomOutSpeed * Time.deltaTime;
        if (GetComponent<Camera>().orthographicSize >= maxZoomLevel){
            GetComponent<Camera>().orthographicSize = maxZoomLevel;
            bzoomOut = false;
        }
    }
    void ZoomIn(){
        GetComponent<Camera>().orthographicSize -= zoomInSpeed * Time.deltaTime;
        if (GetComponent<Camera>().orthographicSize <= minZoomLevel){
            GetComponent<Camera>().orthographicSize = minZoomLevel;
            bzoomIn = false;
        }
    }
    public void StartZoomOut(){
        bzoomIn = false;
        bzoomOut = true; 
    }

    public void StartZoomIn(){
        bzoomOut = false;
        bzoomIn = true;
    }
    #endregion
}











































































