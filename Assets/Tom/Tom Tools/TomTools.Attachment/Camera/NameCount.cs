﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
public class NameCount : MonoBehaviour {
	
	private int count;
	private Dictionary<string, int> counts = new Dictionary<string, int>();
		
	/// <summary>
	/// Adds the name.
	/// </summary>
	/// <param name="name">Name.</param>
	public void AddName(string name)
	{
		
		if (!counts.ContainsKey(name)){

			counts[name] = count;
			count++;


		}

	}
	
	/// <summary>
	/// Returns proportion of parameter name in all calls to AddName.
	/// </summary>
	/// <returns>Double in interval [0, 1]. Returns 0 if AddName has not been called.</returns>
	/// <param name="name">Name.</param>
	public double NameProportion(string name)

	{
		print (counts[name] + " / " + count); 
		return (double)(counts[name]) / count;
	}


	// Use this for initialization
	void Start () {
		
		AddName("James");
		AddName("John");
		AddName("Mary");
		AddName("Mary");
		
		print( NameProportion("John"));
		print( NameProportion("Mary"));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
