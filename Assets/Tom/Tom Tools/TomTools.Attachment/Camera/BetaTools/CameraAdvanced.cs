﻿#region Class Information
/*-------------------------------------------------
 File:           		CameraAdvanced.cs
 Project:          		$projectname$
 Solution:           	$solution$
 Description:        	$description$
                
---------------------------------------------------
Copyright (c) 2014 Thomas Ferrer $Date$
---------------------------------------------------*/
# endregion

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace Tomtools.Attachment{
	[RequireComponent(typeof(Camera))]
	[RequireComponent(typeof(CameraZoom))]
	[RequireComponent(typeof(CameraFollower))]
	[RequireComponent(typeof(CameraMoveTo))]
	public class CameraAdvanced : TomMono {

		#region Public Variables
		public static CameraAdvanced instance;
		public CameraZoom cameraZoom;
		public CameraFollower cameraFollow;
		public CameraMoveTo cameraMoveTo;
		public bool isMoving;

		#endregion

		#region Private Variables

		#endregion

		#region Build - In Unity
		void Awake(){
			instance = this;
		}
		
		void Start () {
			cameraFollow = GetComponent<CameraFollower>();
			cameraMoveTo = GetComponent<CameraMoveTo>();
			cameraZoom = GetComponent<CameraZoom>();

		}
	
		void Update(){
			if (isMoving){
				if (GetCamMoveState() == CameraMoveTo.State.IDLE){
					/* set up the clamp for the next room and start following player */
				
					isMoving = false;
				}
			}
		}
		#endregion

		#region Helpers 

		public void UnClamp(){
			cameraFollow.UnClamp();
		}

		public void Clamp(Transform min, Transform max){
			cameraFollow.Clamp(min,max);
		}

		public void Move(Vector3 _p, Vector3 _min, Vector3 _max){
			isMoving = true;
			cameraMoveTo.SetMove(_p,_min,_max);
		}

		public CameraMoveTo.State GetCamMoveState(){

			return cameraMoveTo.state;
		}


		#endregion
	}
}








































































































