﻿#region Class Information
/*-------------------------------------------------
 File:           		CameraMoveTo.cs
 Project:          		$projectname$
 Solution:           	$solution$
 Description:        	$description$
                
---------------------------------------------------
Copyright (c) 2014 Thomas Ferrer $Date$
---------------------------------------------------*/
# endregion

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Tomtools.Attachment{
	public class CameraMoveTo : TomMono {

		#region Public Variables
		public enum State {
			IDLE,
			MOVING
		}
		public State state;
		public float speed = 2.0f;
		#endregion

		#region Private Variables
		private Vector3 movePoint;
		private Vector3 min;
		private Vector3 max;
		#endregion

		#region Build - In Unity

		void Update () {
			switch (state){
			case State.IDLE:break;
			case State.MOVING:Move ();break;
			}
	
		}
		#endregion
		void Move(){
			float step = speed * Time.deltaTime;
			movePoint.z = -10f;
			transform.position = Vector3.MoveTowards(transform.position,movePoint,step);
			Vector3 p = transform.position;
			Vector2 orthographicSize = new Vector2(GetComponent<Camera>().orthographicSize*GetComponent<Camera>().aspect, GetComponent<Camera>().orthographicSize);
			Vector3 v3min = new Vector3(min.x + orthographicSize.x,min.y + orthographicSize.y);
			Vector3 v3max = new Vector3(max.x - orthographicSize.x, max.y - orthographicSize.y);



			if (p.x >= v3min.x && p.y >= v3min.y && p.x <= v3max.x && p.y <= v3max.y){
				state = State.IDLE;
			}
			float distance = Vector3.Distance(transform.position,movePoint);
			if (distance <= 0.0001f){
				state = State.IDLE;
			}
		}

		public void SetMove(Vector3 _position, Vector3 _min, Vector3 _max){
			state = State.MOVING;
			movePoint = _position;
			min = _min;
			max = _max;
		}
	}
}


























































































