﻿#region Class Information
/*-------------------------------------------------
 File:           		CameraFollow.cs
 Project:          		SpacebumpV1.0
 Solution:           	SpacebumpV1.0-csharp.sln
 Description:        	Follows the "FollowObject"
                        but keeps it's z position.
                
---------------------------------------------------
Copyright (c) 2014 Thomas Ferrer December 18, 2014
---------------------------------------------------*/
# endregion

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraFollower : TomMono {

	#region Public member variables
    [Group("Component Attachments")]
    public Transform FollowObject;
    /* the style that it will follow the game object */
    /* dead zone is a bionic commando style follow 
     * where you get a certain distance of the x and y axis */
    public enum FollowStyle{ BASIC_FOLLOW, LERP_FOLLOW, DEADZONE_FOLLOW};
    [Group("Follow Attributes")]
    public FollowStyle followStyle;
    public enum DeadZoneAxis { X_AXIS = 0, Y_AXIS = 1, BOTH = 2};
    [Group("Follow Attributes")]
    public DeadZoneAxis deadZoneAxis;
    [Group("Follow Attributes")]
    public float fXAxisValue = 2.0f;
    [Group("Follow Attributes")]
    public float fYAxisValue = 2.0f;
    public enum ClampAxis { NONE = 0, X_AXIS = 1, Y_AXIS = 2, BOTH = 3};
    [Group("Follow Attributes")]
    public ClampAxis clampAxis;
    [Group("Follow Attributes")]
    public Transform minLimit;
    [Group("Follow Attributes")]
    public Transform maxLimit;
    [Group("Follow Attributes")]
    public bool bLerpBackToFollowObject = true;
    [Group("Follow Attributes")]
    public float followSpeed = 2.0f;
    [Group("Follow Attributes")]
    public float fastSpeed = 10.0f;
	[Group("Follow Attributes")]
	public float camWidth = 2.0f;

	public bool drawGizmos;

	#endregion

	#region Public member properties

	#endregion

	#region Private member variables
    private bool startLerpingBackX = false;
    private bool startLerpingBackY = false;
    private Vector2 orthographicSize;
    private Vector3 v3MinLimit;
    private Vector3 v3MaxLimit;
	#endregion
	
	#region Private member properties

	#endregion

	#region Unity Built-in Methods

	void Awake(){
       
		if (FollowObject)
       	 	transform.position = new Vector3(FollowObject.position.x, FollowObject.position.y, transform.position.z);

		if (startLerpingBackX)
			print ("startLerpx " + startLerpingBackX);
		if (startLerpingBackY)
			print ("startLerpY " + startLerpingBackY);
	}
        
	void Start(){
      
	}
        
	void Update(){
     
		if (minLimit && maxLimit)
       		Follow();
	}

	#endregion

    #region Helpers
	public void Clamp(Transform min, Transform max){
		minLimit = min;
		maxLimit = max;
	}

	public void UnClamp(){
		minLimit = null;
		maxLimit = null;
	}
    private void Follow(){

		orthographicSize = new Vector2(GetComponent<Camera>().orthographicSize * GetComponent<Camera>().aspect, GetComponent<Camera>().orthographicSize);
        v3MinLimit = new Vector3(minLimit.position.x + orthographicSize.x,minLimit.position.y + orthographicSize.y);
        v3MaxLimit = new Vector3(maxLimit.position.x - orthographicSize.x, maxLimit.position.y - orthographicSize.y);
        float step = followSpeed * Time.deltaTime;
        switch (followStyle){
            case FollowStyle.BASIC_FOLLOW:
                BasicFollow();
                break;
            case FollowStyle.LERP_FOLLOW:
                LerpFollow(step);
                break;
            case FollowStyle.DEADZONE_FOLLOW:
                DeadZoneFollow(step);
                break;
        }
    }

    private void BasicFollow(){



        switch (clampAxis){
            case ClampAxis.NONE: 
                transform.position = new Vector3(FollowObject.position.x, FollowObject.position.y, transform.position.z);
                    break;
            case ClampAxis.X_AXIS: 
                transform.position = new Vector3(Mathf.Clamp(FollowObject.position.x, 
                    v3MinLimit.x, v3MaxLimit.x), 
                    FollowObject.position.y, 
                    transform.position.z);
                break;
            case ClampAxis.Y_AXIS: 
                transform.position = new Vector3(FollowObject.position.x, 
                    Mathf.Clamp(FollowObject.position.y, v3MinLimit.y, v3MaxLimit.y), 
                    transform.position.z);
                break;
            case ClampAxis.BOTH:
			Vector3 newPosition = new Vector3(Mathf.Clamp(FollowObject.position.x, 
                    v3MinLimit.x, v3MaxLimit.x ), 
                    Mathf.Clamp(FollowObject.position.y, 
                        v3MinLimit.y, 
                        v3MaxLimit.y), 
                    transform.position.z);
			//newPosition = new Vector3(Mathf.Ceil(newPosition.x),Mathf.Ceil(newPosition.y),newPosition.z);
			transform.position = newPosition;
                break;
        }
    }
    private void DeadZoneFollow(float _fspeed){
        switch (clampAxis){
            case ClampAxis.NONE: DeadZoneClampNone(_fspeed); break;
            case ClampAxis.X_AXIS:DeadZoneClampX(_fspeed); break;
            case ClampAxis.Y_AXIS: DeadZoneClampY(_fspeed);break;
            case ClampAxis.BOTH:DeadZoneClampBoth(_fspeed); break;
        }
    }
    private void DeadZoneClampNone(float _fspeed){

        if (isOutOfDeadZone()){
            startLerpingBackX = true;
            Vector3 lerpBackPosition = new Vector3(FollowObject.position.x, FollowObject.position.y,transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, lerpBackPosition, _fspeed);
            float distance = Vector3.Distance(transform.position, lerpBackPosition);
            if (distance < 0.001f){
                startLerpingBackX = false;
            }
        }
   
    }
    private void DeadZoneClampX(float _fspeed){
        if (isOutOfDeadZone()){
            startLerpingBackX = true;
            Vector3 lerpBackPosition = new Vector3(Mathf.Clamp(FollowObject.position.x,
                v3MinLimit.x,v3MaxLimit.x), 
                FollowObject.position.y,
                transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, lerpBackPosition, _fspeed);
            float distance = Vector3.Distance(transform.position, lerpBackPosition);
            if (distance < 0.001f){
                startLerpingBackX = false;
            }
        }
    }
    private void DeadZoneClampY(float _fspeed){
        if (isOutOfDeadZone()){
            startLerpingBackX = true;
            Vector3 lerpBackPosition = new Vector3(FollowObject.position.x,
                Mathf.Clamp(FollowObject.position.y,
                    v3MinLimit.y,v3MaxLimit.y), 
                transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, lerpBackPosition, _fspeed);
            float distance = Vector3.Distance(transform.position, lerpBackPosition);
            if (distance < 0.001f){
                startLerpingBackX = false;
            }
        }
    }
    private void DeadZoneClampBoth(float _fspeed){
        if (isOutOfDeadZone()){
            startLerpingBackX = true;
            Vector3 lerpBackPosition = new Vector3(Mathf.Clamp(FollowObject.position.x,
                v3MinLimit.x,v3MaxLimit.x), 
                Mathf.Clamp(FollowObject.position.y,
                    v3MinLimit.y,v3MaxLimit.y), 
                transform.position.z);
            transform.position = Vector3.MoveTowards(transform.position, lerpBackPosition, _fspeed);
            float distance = Vector3.Distance(transform.position, lerpBackPosition);
            if (distance < 0.001f){
                startLerpingBackX = false;
            }
        }
    }
    private void LerpFollow(float _fspeed){

        Vector3 _v3NoneClamped = new Vector3(FollowObject.position.x,FollowObject.position.y,transform.position.z);
        Vector3 _v3Clamped = new Vector3(Mathf.Clamp(FollowObject.position.x, v3MinLimit.x, v3MaxLimit.x),
            Mathf.Clamp(FollowObject.position.y,v3MinLimit.y,v3MaxLimit.y),transform.position.z);
        Vector3 _v3YClamped = new Vector3(FollowObject.position.x,
            Mathf.Clamp(FollowObject.position.y,v3MinLimit.y,v3MaxLimit.y),transform.position.z);
        Vector3 _v3XClamped = new Vector3(Mathf.Clamp(FollowObject.position.x, v3MinLimit.x, maxLimit.position.x),
            FollowObject.position.y,transform.position.z);

        switch (clampAxis){
            case ClampAxis.NONE: 

                transform.position = Vector3.Lerp(transform.position,_v3NoneClamped,_fspeed);
                break;
            case ClampAxis.X_AXIS: 
                XReset();
                transform.position = Vector3.Lerp(transform.position,_v3XClamped,_fspeed);
                break;
            case ClampAxis.Y_AXIS: 
                YReset();
                transform.position = Vector3.Lerp(transform.position,_v3YClamped,_fspeed);
                break;
            case ClampAxis.BOTH:
                XReset();
                YReset();
                transform.position = Vector3.Lerp(transform.position,_v3Clamped,_fspeed);
                break;
        }
    }

    private void XReset(){
        if (transform.position.x > v3MaxLimit.x){
            Vector3 _v3Reset = new Vector3(v3MaxLimit.x, transform.position.y, transform.position.z);
            transform.position = _v3Reset;
        }
        if (transform.position.x < v3MinLimit.x){
            Vector3 _v3Reset = new Vector3(v3MinLimit.x, transform.position.y, transform.position.z);
            transform.position = _v3Reset;
        }
    }
    private void YReset(){
        if (transform.position.y > v3MaxLimit.y){
            Vector3 _v3Reset = new Vector3( transform.position.x,v3MaxLimit.y, transform.position.z);
            transform.position = _v3Reset;
        }
        if (transform.position.y < v3MinLimit.y){
            Vector3 _v3Reset = new Vector3( transform.position.x,v3MinLimit.y, transform.position.z);
            transform.position = _v3Reset;
        }
    }

    private bool isOutOfDeadZone(){

        float xDistance = Mathf.Abs(transform.position.x - FollowObject.position.x);
        float yDistance = Mathf.Abs(transform.position.y - FollowObject.position.y);
        if (xDistance >= fXAxisValue){
            return true;
        }
        if (yDistance >= fYAxisValue){
            return true;
        }
        return false;
    }
  
    #endregion


	#region ONDRAWGIZMOS

	void OnDrawGizmos(){
		if (drawGizmos){
			Gizmos.color = Color.red;
			Gizmos.DrawWireCube(transform.position,new Vector3((GetComponent<Camera>().orthographicSize * GetComponent<Camera>().aspect) * 2f,GetComponent<Camera>().orthographicSize * 2f,GetComponent<Camera>().orthographicSize));
		}
	}
	#endregion
}











































































