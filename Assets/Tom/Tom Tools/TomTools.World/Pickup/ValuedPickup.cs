﻿#region Class Information
/*-------------------------------------------------
 File:           		ValuedPickup.cs
 Project:          		$projectName$
 Solution:           	$solutionName$
 Description:        	$description$


    Name            Date        Version Description                    
---------------------------------------------------
Thomas Ferrer      $date$             1.0
---------------------------------------------------*/
# endregion

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

        
public class ValuedPickup : PickupBase {

	#region Public member properties
    [Group("Valued Attributes")]
    public float fvalue = 1.0f;
    [Group("Attachments")]
    public List<ObjectAttachment> objectsToAttach
    = new List<ObjectAttachment>();
	#endregion

	#region Private member properties
	#endregion

    #region Serilized Class
    [System.Serializable]
    public class ObjectAttachment{
        public GameObject obj;
        public string action;
    }
    #endregion
	
	#region Unity Built-in Methods

	protected override void Awake(){
      
       
	}
        
	protected override void Start(){

     
	}
        
    protected override void Update(){
		if (collisionType == CollisionType.DISTANCE){
			DistanceCheck();
		}
	}

    protected virtual void InvokeActions(){
//        foreach (ObjectAttachment attachment in objectsToAttach){
//            //attachment.obj.SendMessage(attachment.action,fvalue);
//        }

		print ("invoke actions has nothing in it");
    }

    protected virtual void UseCollisionBehaviour(){
        switch (collisionBehavior)
        {
            case CollisionBehavior.NOTHING:print ("you have collision behaviour set as nothing"); break;
            case CollisionBehavior.POOL_DESTROY:Pool_Destroy(gameObject);
		
			break;

        }

		InvokeActions();
		UseCollisionBehaviour();
    }

	#endregion

	protected override void DistanceCheck(){
		foreach (Transform t in distanceCheckTargets){
			if (t){
				float distance = Vector3.Distance(t.position,transform.position);

				if (distance < fDistance){
					InvokeActions();
					UseCollisionBehaviour();
				}
			}
		}
	}
	
	protected override void OnTriggerEnter2D(Collider2D other){
		
		if (isCollidable(other.transform)&& collisionType == CollisionType.TRIGGER){

			InvokeActions();
			UseCollisionBehaviour();
		}
	}
	
	protected override void OnCollisionEnter2D(Collision2D other){
		
		if (isCollidable(other.transform) && collisionType == CollisionType.COLLIDER){
			InvokeActions();
			UseCollisionBehaviour();
		}
	}
}











































































