﻿#region Class Information
/*-------------------------------------------------
 File:           		Pickup.cs
 Project:          		Spacebump
 Solution:           	Assembly-CSharp-vs
 Description:        	The base class for all
						pick ups in the game. This
						minimal class lets you choose
						how the pick up acts and who
						can pick it up dependent on
						layers or tags.

    Name            Date        Version Description                    
---------------------------------------------------
Thomas Ferrer  December 17, 2014      1.0
---------------------------------------------------*/
# endregion

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/* Base class for all pickups */
public class PickupBase : TomMono {


	#region Public member properties
	public enum CollisionType {	DISTANCE, TRIGGER, COLLIDER};
	public CollisionType collisionType;

    /* Use a layermask, tags or both for collisions with this pick up*/
    public enum CollisionOption { LAYERMASK, TAGS, BOTH};
    /* The enum variable you will set for the collision*/
    [Group("Collision Attributes")]
    public CollisionOption collisionOption;
    /* The behavior of the collision once it's touched*/
   // [Group("Collision Attributes")]
    public enum CollisionBehavior { NOTHING, POOL_DESTROY , DESTROY, DESTROY_LOAD_NEW, POOL_DESTROY_LOAD_NEW };
    /* the enum variable you will set for the behavior */
    [Group("Collision Attributes")]
    public CollisionBehavior collisionBehavior;
    /* the layer to be set on the collision */
    [Group("Collision Attributes")]
    public LayerMask collisionMask = 1;
    /* tags to be set for  collision*/
    [Group("Collision Attributes")]
    public List<string> tagsToCollideWith;
	/* tags to be set for  collision*/
	[Group("Collision Attributes")]
	public float fDistance = 1.0f;
	/* tags to be set for  collision*/
	[Group("Collision Attributes")]
	public List<Transform> distanceCheckTargets= new List<Transform>();

  
	#endregion

	#region Private member properties

	#endregion
	
	#region Unity Built-in Methods

	protected virtual void Awake(){

	}
        
	protected virtual void Start(){
     
	}
        
	protected virtual void Update(){
     
		if (collisionType == CollisionType.DISTANCE){
			DistanceCheck();
		}
	}

	#endregion

    #region Collision functions

	protected virtual void DistanceCheck(){
		foreach (Transform t in distanceCheckTargets){
			if (t){
				float distance = Vector3.Distance(t.position,transform.position);
				if (distance < fDistance){
					print ("you have collided with me ");
				}
			}
		}
	}

    protected virtual void OnTriggerEnter2D(Collider2D other){

		if (isCollidable(other.transform)&& collisionType == CollisionType.TRIGGER){
            print("you have collided with me!");
        }
    }

	protected virtual void OnCollisionEnter2D(Collision2D other){
		
		if (isCollidable(other.transform) && collisionType == CollisionType.COLLIDER){
			print("you have collided with me!");
		}
	}


    protected virtual bool isCollidable(Transform other){

        switch(collisionOption){

            case CollisionOption.LAYERMASK:
				Vector3 v3Direction = other.position - transform.position;
				//VectorLine.SetRay(Color.green,transform.position,v3Direction);
				RaycastHit2D hit = Physics2D.Raycast(transform.position,v3Direction,1f,collisionMask);
	            if (hit.transform){
					
					return true;
				}
		
                break;
            case CollisionOption.TAGS:
                foreach (string tag in tagsToCollideWith){
                    if (tag == other.tag){
                        return true;
                    }
                }
                break;
            case CollisionOption.BOTH:
                if (Physics2D.Linecast(transform.position,other.position,collisionMask)){return true;}
                else {
                    foreach (string tag in tagsToCollideWith){
                        if (tag == other.tag){
                            return true;
                        }
                    }
                }
                break;
        }

        return false;
      
    }

    #endregion
}











































































