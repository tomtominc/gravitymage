﻿#region Class Information
/*-------------------------------------------------
 File:           		HUDFPS.cs
 Project:          		Tomtool.Utils.HUD namespace
 Solution:           	Tomtool V0.02a
 Description:        	Writes out a HUD for the 
 						frame rate.


    Name            Date        Version Description                    
---------------------------------------------------
Thomas Ferrer      $date$             1.0
---------------------------------------------------*/
# endregion

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Tomtools.Utils.HUD{
	public class HUDFPS : TomMono 
	{
		[Group("HUD - frame rate reduced if used")]
		public Text FPS;
		[Group("Inspector view - frame rate more accurate")]
		public string fpsTrack = "0";

		public  float updateInterval = 0.5F;
		
		private float accum   = 0; // FPS accumulated over the interval
		private int   frames  = 0; // Frames drawn over the interval
		private float timeleft; // Left time for current interval
		
		void Start()
		{
			timeleft = updateInterval;  
		}
		
		void Update()
		{
			timeleft -= Time.deltaTime;
			accum += Time.timeScale/Time.deltaTime;
			++frames;
			
			// Interval ended - update GUI text and start new interval
			if( timeleft <= 0.0 )
			{
				// display two fractional digits (f2 format)
				float fps = accum/frames;
				string format = System.String.Format("{0:F2} FPS",fps);
				if (FPS) FPS.text = format;
				fpsTrack = format;
				timeleft = updateInterval;
				accum = 0.0F;
				frames = 0;
			}
		}
	}
}











































































