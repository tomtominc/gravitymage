﻿#region Class Information
/*-------------------------------------------------
 File:           		GameObjectPoolManager.cs
 Project:          		TomTool.Utils.V3D namespace
 Solution:           	$solutionName$
 Description:        	


    Name            Date        Version Description                    
---------------------------------------------------
Thomas Ferrer   December 4, 2014       1.0
---------------------------------------------------*/
# endregion

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tomtools.Utils.Gameplay;
/* Script that raycasts out and shoots a gameObject in that direction
 * Can only use an Object with Effects settings attached to it */

namespace Tomtools.Utils{
	public class RayCaster : TomMono {

        /* Instance of class creates a gameplaymodule to store all data */
		private static RayCaster instance;
        static public RayCaster Instance{

            get{
                if (!instance){
                    GameplayModule gamePlayModule = GameplayModule.Instance;
                    GameObject gameModule = gamePlayModule.gameObject;
                    gameModule.AddComponent<RayCaster>();
                    instance = gameModule.GetComponent<RayCaster>();
                    return instance;

                }
                else return instance;

            }
        }

        /// <summary>
        /// Shoots a ray from "_vFromLocation" in the direction of "_vRayDirection"
        /// only shoots at the distance of OPTIONAL "_fDistance" and if it doesn't hit anything
        /// you can still get a Vector3 "_vRayDirection" * 1000 if OPTIONAL "_bReturnsIfNothingHits" = true
        /// otherwise if it doesn't hit anything it will return Vector3.zero.
        /// </summary>
        /// <returns>The shoot ray.</returns>
        /// <param name="_vFromLocation">V from location.</param>
        /// <param name="_vRayDirection">V ray direction.</param>
        /// <param name="_fDistance">F distance.</param>
        /// <param name="_bReturnsIfNothingHits">If set to <c>true</c> b returns if nothing hits.</param>
        public Vector3 ShootRay(Vector3 _vFromLocation,Vector3 _vRayDirection, 
            float _fDistance = Mathf.Infinity, bool _bReturnsIfNothingHits = true){

			RaycastHit hitInfo;
            if (Physics.Raycast(_vFromLocation, _vRayDirection, out hitInfo)){

                print(hitInfo.transform.position);
                return hitInfo.transform.position;
            }
            else if (_bReturnsIfNothingHits){

                print(_vRayDirection * 1000);
                return _vRayDirection * 1000;
			}
			else{
                print ("raycast returning null && is returning Vector3.zero");
                return Vector3.zero;
			}
		}


	}
}
