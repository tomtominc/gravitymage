﻿#region Class Information
/*-------------------------------------------------
 File:           		GameplayModule.cs
 Project:          		$projectName$
 Solution:           	$solutionName$
 Description:        	The top class for all the
                        Gameplay components.


    Name            Date        Version Description                    
---------------------------------------------------
Thomas Ferrer      $date$             1.0
---------------------------------------------------*/
# endregion

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Tomtools.Utils.Gameplay{

	public class GameplayModule : TomMono {

		#region Public member properties
		private static GameplayModule instance;
		static public GameplayModule Instance{
			
			get{
				if (!instance){
					GameObject gameModule = GameObject.Find("[Gameplay Module]");
					if (gameModule){
						gameModule.AddComponent<GameplayModule>();
						instance = gameModule.GetComponent<GameplayModule>();
						return instance;
					}
					else{
						GameObject go = new GameObject();
						go.transform.position = Vector3.zero;
						go.name = "[Gameplay Module]";
						instance = new GameplayModule();
						go.AddComponent<GameplayModule>();
						instance = go.GetComponent<GameplayModule>();
						return instance;
					}
				}
				else return instance;
				
			}
		}
            
        /* Holds a dictionary of all the gameObjects that use
         * the gameplay components */
		public static Dictionary<int, GameObject>  dModObjs
			= new Dictionary<int, GameObject>();
        /* Holds a dictionary of a list of grouped gameObjects that use
         * the gameplay components organized into tags 
         * This is a great for moving a group of objects at once or 
         * calling a function on all objects in a group.
        */
        public static Dictionary<string, List<GameObject>>  dModObjGroups
        = new Dictionary<string, List<GameObject>>();

        /// <summary>
        /// Adds a gameObject to the dictionarys 
        /// returns true if the module was addes
        /// false otherwise
        /// </summary>
        /// <returns><c>true</c>, if module was added, <c>false</c> otherwise.</returns>
        /// <param name="_go">Go.</param>
        public bool AddModule(GameObject _go){

            /* Gets the object id of the gameObject that 
             * used the "Proc_MoveToPoint" function */
            int _nObjectId = _go.GetInstanceID();
            /* checks in the GameplayModules dictionary to 
             * see if the object has been added to the dictionary */
            if (!dModObjs.ContainsKey(_nObjectId)){
                dModObjs.Add(_nObjectId,_go);
               /* check if the group has been created 
                * if it doesn't create the group and add a new list
                * else just add it to the existing group */
                if (!dModObjGroups.ContainsKey(_go.tag)){

                    List<GameObject> _LNewGameObjectList = 
                        new List<GameObject>();
                    _LNewGameObjectList.Add(_go);
                    dModObjGroups.Add(_go.tag, _LNewGameObjectList);
                }
                else {
                    dModObjGroups[_go.tag].Add(_go);
                }
                return true;
            }

            return false;
        }

		#endregion

		#region Private member properties

		#endregion

	}
}











































































