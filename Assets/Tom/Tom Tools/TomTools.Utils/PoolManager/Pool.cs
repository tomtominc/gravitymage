﻿#region Class Information
/*-------------------------------------------------
 File:           		Pool.cs
 Project:          		TomTool.Utils namespace
 Solution:           	$solutionName$
 Description:        	Pool mananger that handles
 						destroying and instantiating
						like unity does only it sets
						objects active or inactive.


    Name            Date        Version Description                    
---------------------------------------------------
Thomas Ferrer   December 4, 2014       1.0
---------------------------------------------------*/
# endregion

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Tomtools.Utils{

	public class Pool : TomMono {

		#region Public member properties
		private static Pool instance;
		public static Pool Instance{

			get{
				if (!instance){
					GameObject poolManager = new GameObject();
					poolManager.name = "[Pool Mananger]";
					poolManager.AddComponent<Pool>();
					instance = poolManager.GetComponent<Pool>();
					return instance;
				}
				else return instance;
			}
		}
		/* List of dictionarys that hold active gameObjects by name */
		private static Dictionary<string,List<GameObject>> LActiveGameObjects
			= new Dictionary<string,List<GameObject>>();
		/* list of dictionarys that hold inactive gameObjects by name */
		private static Dictionary<string,Queue<GameObject>> LInActiveGameObjects
			= new Dictionary<string,Queue<GameObject>>();

		public int instantiateCount = 0;
		public int totalCount = 0;

		#endregion

		#region Private member properties

		#endregion
		
		#region Unity Built-in Method

		
        public GameObject InstantiateGO(GameObject _go,  Vector3 _vPosition = default(Vector3),
            Quaternion _qQuaternion = default(Quaternion)){

			totalCount++;
			string _sGameObjectName = _go.name;
			if (LInActiveGameObjects.ContainsKey(_sGameObjectName)){
				if (LInActiveGameObjects[_sGameObjectName].Count > 0){
					if (LInActiveGameObjects[_sGameObjectName].Peek()){
						GameObject _activeGo = LInActiveGameObjects[_sGameObjectName].Dequeue();
						_activeGo.transform.position = _vPosition;
						_activeGo.transform.rotation = _qQuaternion;
						_activeGo.SetActive(true);

						if (LActiveGameObjects.ContainsKey(_sGameObjectName)){
							
							LActiveGameObjects[_sGameObjectName].Add(_activeGo);

						}
						else{
							List<GameObject> newList = new List<GameObject>();
							newList.Add(_activeGo);
							LActiveGameObjects.Add(_sGameObjectName,newList);
						}

						return _activeGo;
					}
					else{
						
                        return InstantiateNormal(_go,_vPosition,_qQuaternion);
					}
				}
				else{
					
					return InstantiateNormal(_go,_vPosition,_qQuaternion);
				}
			}
			else{
				return InstantiateNormal(_go,_vPosition,_qQuaternion);
			}

			
		}

		private GameObject InstantiateNormal(GameObject _go, Vector3 _vPosition, Quaternion _qQuaternion){

			GameObject _activeGo = Instantiate(_go,_vPosition,_qQuaternion) as GameObject;
			string _sGameObjectName = _activeGo.name = _activeGo.name.Replace("(Clone)","").Trim();
			
			if (LActiveGameObjects.ContainsKey(_sGameObjectName)){
				
				LActiveGameObjects[_sGameObjectName].Add(_activeGo);
			}
			else{
				List<GameObject> newList = new List<GameObject>();
				newList.Add(_activeGo);
				LActiveGameObjects.Add(_sGameObjectName,newList);
			}

			instantiateCount++;
			return _activeGo;
		}

		public void DestroyGO(GameObject _go, float _fSeconds = 0f){
           // print ("object to destory = "+ _go);
			StartCoroutine(DestroyGOAfterSeconds(_go,_fSeconds));
		}

		private IEnumerator DestroyGOAfterSeconds(GameObject _go, float _fSeconds){

			yield return new WaitForSeconds(_fSeconds);
			string _sGameObjectName = _go.name;
			_go.SetActive(false);
			if (LActiveGameObjects.ContainsKey(_sGameObjectName)){
				
                LActiveGameObjects[_sGameObjectName].Remove(_go);

				if (LInActiveGameObjects.ContainsKey(_sGameObjectName)){
					LInActiveGameObjects[_sGameObjectName].Enqueue(_go);
				}
				else{
					Queue<GameObject> newQueue = new Queue<GameObject>();
					newQueue.Enqueue(_go);
					LInActiveGameObjects.Add(_sGameObjectName,newQueue);
				}
			}
			else{

				if (LInActiveGameObjects.ContainsKey(_sGameObjectName)){
					LInActiveGameObjects[_sGameObjectName].Enqueue(_go);
				}
				else{
					Queue<GameObject> newQueue = new Queue<GameObject>();
					newQueue.Enqueue(_go);
					LInActiveGameObjects.Add(_sGameObjectName,newQueue);
				}
			}
		}

		#endregion
	}
}











































































