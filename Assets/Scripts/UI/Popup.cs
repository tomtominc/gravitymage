﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class Popup : MonoBehaviour {

	public static Popup instance;
	public Animator anim;
	public Button yesButton;
	public Button noButton;

	private void Awake(){
		anim = GetComponent<Animator>();
	}


	public Text Question;
	public Action AOnNoFunc;
	public Action AOnYesFunc;
	public delegate void NoFunc();
	public event NoFunc eNoFunc;
	public void OnNoFunc(){
		if (eNoFunc != null)
			eNoFunc();

		if (AOnNoFunc != null){
			AOnNoFunc.Invoke();
		}

		Button[] buttons = FindObjectsOfType<Button>();
		foreach (Button b in buttons){
			b.enabled = true;
		}
	}

	public delegate void YesFunc();
	public event YesFunc eYesFunc;
	public void OnYesFunc(){
		if (eYesFunc != null)
			eYesFunc();

		if (AOnYesFunc != null){
			AOnYesFunc.Invoke();
		}

		Button[] buttons = FindObjectsOfType<Button>();
		foreach (Button b in buttons){
			b.enabled = true;
		}
	}

	public void InitPopup(string _question, Action YesAnswer ,Action NoAnswer, bool canPressOtherButtons){
		if (NoAnswer == null)
						throw new ArgumentNullException ("NoAnswer");

		Question.text = _question;
		AOnNoFunc = new Action(NoAnswer);
		AOnYesFunc = new Action(YesAnswer);


		Button[] buttons = FindObjectsOfType<Button>();
		foreach (Button b in buttons){
			b.enabled = canPressOtherButtons;
		}

		yesButton.enabled = true;
		noButton.enabled = true;
		anim.SetTrigger("MoveIn");
	}

}

















































