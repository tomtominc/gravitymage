﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIHelper : MonoBehaviour {

	public AudioClip InSound;
	public AudioClip OutSound;

	public AudioClip buttonClick;
	private GameObject MoveObject;
	private Transform moveTarget; 
	private Text textToChange;
	private GameObject UIToSetActive;
	public Text EnviornmentTitleText;

	//trigger anim
	private Animator animToTrigger;
	private string animTriggerName;
	////

	public Popup popup;

	public void TriggerAnimPart1(Animator anim){
		animToTrigger = anim;
	}

	public void TriggerAnimPart2(string animTriggerN){
		animTriggerName = animTriggerN;
	}

	public void TriggerAnimPart3(float s){
		StartCoroutine(ETriggerAnim(s));
	}

	IEnumerator ETriggerAnim(float s){
		yield return new WaitForSeconds(s);
		animToTrigger.SetTrigger(animTriggerName);
		
	}

	public void DelayedSetUIActiveP2(float seconds){
		StartCoroutine(EDelaySetUIActive(seconds));
	}

	public void DelayedSetUIActiveP1(GameObject go){
		UIToSetActive = go;
	}

	IEnumerator EDelaySetUIActive(float s){
		yield return new WaitForSeconds(s);
		UIToSetActive.SetActive(true);

	}

	public void SetActiveSelfFalse(){
		gameObject.SetActive(false);
	}

	public void Popup_QuitGame(){

		popup.InitPopup("Quit the game?",QuitGame,PopupMoveOut,false);
	}

	public void PopupMoveOut(){
		popup.anim.SetTrigger("MoveOut");
	}

	public void QuitGame(){
		Application.Quit();
	}

	public void PlayInSound(){
		if (InSound)
		AudioSource.PlayClipAtPoint(InSound,Vector3.zero);
	}

	public void PlayOutSound(){
		if (OutSound)
		AudioSource.PlayClipAtPoint(OutSound,Vector3.zero);
	}


	public void PlayClickSound(){
		if (buttonClick)
		AudioSource.PlayClipAtPoint(buttonClick,Vector3.zero);
	}

	public void EnableUI(GameObject obj){
		obj.SetActive(true);

	}

	public void DisableUI(GameObject obj){
		obj.SetActive(false);
	}

	public void SetEnviornmentText(string _evo){
		if (_evo == "Starving")
			_evo = "Starve";
		GameManager.instance.ENVIORNMENT = _evo;
	}

	public void SetEnviornmentTextOnTextObject(){
		EnviornmentTitleText.text = GameManager.instance.ENVIORNMENT;
	}
	public void ChangeTextPart1(Text t){
		textToChange = t;
	}

	public void ChangeTextPart2(string newText){

		if (textToChange){
			textToChange.text = newText;
			textToChange = null;
		}
	}

	public void MoveCursorPart1(GameObject go){
		MoveObject = go;
		//moveSpeed = speed;
		//moveTarget = target;
	}

	public void MoveCursorPart2(Transform target){

		moveTarget = target;

		if (MoveObject){
			MoveObject.transform.position = moveTarget.position;
		}
	}

	private void Update(){


	}
}
