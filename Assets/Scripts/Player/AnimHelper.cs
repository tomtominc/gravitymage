﻿using UnityEngine;
using System.Collections;

public class AnimHelper : TomMono {

	private HeroController controller;

	public void GoToStateIdle(){
		if (!controller)
			controller = transform.parent.GetComponent<HeroController>();

		controller.GoToStateIdle();
	}

	public void GoToStateMidAir(){
		if (!controller)
			controller = transform.parent.GetComponent<HeroController>();
		
		controller.GoToStateMidair();
	}
}
