﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* Hero collision */
/* Handles the collision between "Platform" objects */
/* every pick up is handled by a platform */
/* also has helper functions that help with this */

public class HeroCollision : MonoBehaviour {

	[System.Serializable]
	public class CollisionReaction{

		public enum ReactHitPoint{
			EVERYTHING,
			FEETONLY,
			SIDEONLY,
			HEADONLY
		}
		public ReactHitPoint reactHitPoint;
		public PhysicsMaterial2D materialToReact;
		public Transform particleEffect;
		public AudioClip clip;
		public enum SoundVolume{
			LOW,
			MID,
			HIGH
		}

		public SoundVolume soundVolume;
		public void React(Vector3 hitPoint, bool footCollision, bool headCollision, bool sideCollision){

			switch (reactHitPoint){
			case ReactHitPoint.EVERYTHING:
				HandleSoundReaction(hitPoint);
				break;
			case ReactHitPoint.FEETONLY:
				if (footCollision){
					HandleSoundReaction(hitPoint);
				}
				break;
			}
		}

		private  void HandleSoundReaction(Vector3 hitPoint){
			if (clip != null){
				switch (soundVolume){
				case SoundVolume.LOW:
					hitPoint.z = - 150f;
					AudioSource.PlayClipAtPoint(clip,hitPoint);
					break;
				case SoundVolume.MID:
					hitPoint.z = - 10f;
					AudioSource.PlayClipAtPoint(clip,hitPoint);
					break;
				case SoundVolume.HIGH:
					AudioSource.PlayClipAtPoint(clip,hitPoint);
					break;
				}
				
			}
		}
	}

	public List<CollisionReaction> Collisions
		= new List<CollisionReaction>();

	public float RestartTime = 2f;

	private HeroController controller;
	private HeroPhysics physics;

	private void Start(){
		controller = GetComponent<HeroController>();
		physics = GetComponent<HeroPhysics>();
	}
	private void OnTriggerEnter2D(Collider2D other){

		Platform platform = other.transform.GetComponent<Platform>();
		if (platform != null){
			platform.DoAction(controller,physics,this);
		}
	}

	private void HandleMaterialCollision(PhysicsMaterial2D material,Vector3 hitPoint,bool footC, bool headC, bool sideC){

		foreach (CollisionReaction c in Collisions){
			if (c.materialToReact ==  material){
				c.React(hitPoint,footC,headC,sideC);
			}
		}

	}

	#region HELPERS

	public void HeroWin(){

		controller.GoToStateWin();
		StartCoroutine("GoToNextLevel");

	}

	public void HeroDie(){

		controller.GoToStateDie();
		GetComponent<Collider2D>().enabled = false;
		Vector2 gravity = physics.RayDirection[physics.G_DIRECTION.ToString()] * GameManager.instance.GRAVITY;
		Physics2D.gravity = gravity;
		GetComponent<Rigidbody2D>().gravityScale = 1;
		GetComponent<Rigidbody2D>().AddForce(-gravity * 20f);
		StartCoroutine("Restart");
	}

	IEnumerator GoToNextLevel(){
		
		yield return new WaitForSeconds(RestartTime);
		GameManager.instance.GoToNextLevel();
	}

	IEnumerator Restart(){

		yield return new WaitForSeconds(RestartTime);
		Application.LoadLevel(Application.loadedLevel);
	}
	#endregion
}
