﻿using UnityEngine;
using System.Collections;

public class HeroAnimator : TomMono {

	private HeroController controller;
	private HeroPhysics physics;

	private Animator animator;

	public void Start(){
		controller = GetComponent<HeroController>();
		physics = GetComponent<HeroPhysics>();
		animator = physics.HeroAvatar.GetComponent<Animator>();
	}

	private void Update(){

		animator.SetInteger("STATE",controller.GetState());
	}

}
