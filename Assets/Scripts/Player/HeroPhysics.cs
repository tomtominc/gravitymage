﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum GRAVITYDIRECTION{
	UP,
	DOWN,
	LEFT,
	RIGHT
}

/* Hero physics */
/* This will ray cast in the direction gravity is currently going */
/* when it hits something it will then move it to that location */
/* the rotation of the sprite will depend on what the gravity is at */
/* it will then try and rotate the sprite to that in x amount of seconds */
/* x is whatever the time it will take ( the length of the ray from the hero to the hit point */
public class HeroPhysics : TomMono {


	public GRAVITYDIRECTION G_DIRECTION;
	public LayerMask floorMask;

	[Group("Components")]
	public Transform HeroAvatar;

	[Group("Properties")]
	public float FrontLayerFallSpeed = 1f;
	[Group("Properties")]
	public float BackLayerFallSpeed = 1f;

	private float fallSpeed;
	[Group("Properties")]
	public float rotateSpeed;

	public bool updateEnabled = true;

	private HeroController controller;
	[Group("Properties")]
	public float width = 0.16f;
	[Group("Properties")]
	public float height = 0.16f;
	private bool isBackgroundLayer = false;

	[HideInInspector]
	public float distanceFromLandPoint;

	public Dictionary<string,Vector2> RayDirection
		= new Dictionary<string, Vector2>();

	private Dictionary<string,Vector3> SpriteRotation
		= new Dictionary<string, Vector3>();

	private Dictionary<string,Vector2> DesiredOffset
		= new Dictionary<string, Vector2>();

	private void Start(){
		RayDirection.Add(GRAVITYDIRECTION.UP.ToString(),Vector2.up);
		RayDirection.Add(GRAVITYDIRECTION.DOWN.ToString(),-Vector2.up);
		RayDirection.Add(GRAVITYDIRECTION.LEFT.ToString(),-Vector2.right);
		RayDirection.Add(GRAVITYDIRECTION.RIGHT.ToString(),Vector2.right);

		SpriteRotation.Add(GRAVITYDIRECTION.UP.ToString(),new Vector3(0,0,180f));
		SpriteRotation.Add(GRAVITYDIRECTION.DOWN.ToString(),new Vector3(0,0,0));
		SpriteRotation.Add(GRAVITYDIRECTION.LEFT.ToString(),new Vector3(0,0,270f));
		SpriteRotation.Add(GRAVITYDIRECTION.RIGHT.ToString(),new Vector3(0,0,90f));

		DesiredOffset.Add(GRAVITYDIRECTION.UP.ToString(),new Vector2(0,-(height/2f)));
		DesiredOffset.Add(GRAVITYDIRECTION.DOWN.ToString(),new Vector2(0,height/2f));
		DesiredOffset.Add(GRAVITYDIRECTION.LEFT.ToString(),new Vector2(width/2f,0));
		DesiredOffset.Add(GRAVITYDIRECTION.RIGHT.ToString(),new Vector2(-(width/2f),0));

		G_DIRECTION = GRAVITYDIRECTION.DOWN;
		controller = GetComponent<HeroController>();
		fallSpeed = FrontLayerFallSpeed;

	}

	private void Update(){
		if (updateEnabled){
			Controls ();
			RaycastToFloor();
		}
	}
	public void RaycastToFloor(){

		string gravDirection = G_DIRECTION.ToString();

		RaycastHit2D hit = Physics2D.Raycast((Vector2)transform.position,RayDirection[gravDirection],Mathf.Infinity,floorMask);
		if (hit.transform){

			Vector2 dOffset = DesiredOffset[gravDirection];

			if (controller.isInBackground){
				dOffset.x = dOffset.x * 0.5f;
				dOffset.y = dOffset.y * 0.5f;
			}

			// used for multiplying out for the width and height of the object compared to the point
			Vector3 DesiredPoint = hit.point + dOffset;

			//check distance between desired point and actual position to see if hero is about to land
			distanceFromLandPoint = Vector3.Distance(transform.position,DesiredPoint);

			if (distanceFromLandPoint < 0.1f && !controller.Grounded){
				controller.GoToStateLand();
			}
			if (distanceFromLandPoint > 0.1f){
				controller.GoToStateMidair();
			}
		
			float step = fallSpeed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position,DesiredPoint,step);
			step = rotateSpeed * Time.deltaTime;
			transform.eulerAngles = Vector3.Lerp(transform.eulerAngles,SpriteRotation[gravDirection],step);

		}
	}
	

	public void GoToBackLayer(){
		floorMask =  (1 << 8) | (1 << 9);
		fallSpeed = BackLayerFallSpeed;
		width = 0.10f;
		height = 0.10f;
		BoxCollider2D box2D = GetComponent<BoxCollider2D>();
		box2D.size = new Vector2(width,height);
	}

	public void GotoFrontLayer(){
		floorMask =  (1 << 8);
		fallSpeed = FrontLayerFallSpeed;
		width = 0.16f;
		height = 0.16f;
		BoxCollider2D box2D = GetComponent<BoxCollider2D>();
		box2D.size = new Vector2(width,height);
	}


	public void ChangeDirection(){

		controller.GoToStateSwitchGravity();
		if (G_DIRECTION == GRAVITYDIRECTION.DOWN) {
			G_DIRECTION = GRAVITYDIRECTION.UP;
		}
		else if (G_DIRECTION == GRAVITYDIRECTION.UP) {
			G_DIRECTION = GRAVITYDIRECTION.DOWN;
		}

		else if (G_DIRECTION == GRAVITYDIRECTION.LEFT) {
			G_DIRECTION = GRAVITYDIRECTION.RIGHT;
		}
		else if (G_DIRECTION == GRAVITYDIRECTION.RIGHT) {
			G_DIRECTION = GRAVITYDIRECTION.LEFT;
		}
	}

	public void ChangeGravityOrientation(){

		if (G_DIRECTION == GRAVITYDIRECTION.DOWN) {
			G_DIRECTION = GRAVITYDIRECTION.LEFT;
		}
		else if (G_DIRECTION == GRAVITYDIRECTION.UP) {
			G_DIRECTION = GRAVITYDIRECTION.RIGHT;
		}
		
		else if (G_DIRECTION == GRAVITYDIRECTION.LEFT) {
			G_DIRECTION = GRAVITYDIRECTION.DOWN;
		}
		else if (G_DIRECTION == GRAVITYDIRECTION.RIGHT) {
			G_DIRECTION = GRAVITYDIRECTION.UP;
		}

		print (G_DIRECTION.ToString());
	}

	public void Controls(){
		if (Input.GetKeyDown (KeyCode.Space)) {
			if (controller.Grounded)
				ChangeDirection();
		}
	}
	public void Test(){

		if (Input.GetKeyDown(KeyCode.A)){
			if (controller.Grounded){
				G_DIRECTION = GRAVITYDIRECTION.LEFT;
				controller.GoToStateSwitchGravity();
			}

		}
		if (Input.GetKeyDown(KeyCode.D)){
			if (controller.Grounded){
				G_DIRECTION = GRAVITYDIRECTION.RIGHT;
				controller.GoToStateSwitchGravity();
			}
		}
		if (Input.GetKeyDown(KeyCode.W)){
			if (controller.Grounded){
				G_DIRECTION = GRAVITYDIRECTION.UP;
				controller.GoToStateSwitchGravity();
			}
		}
		if (Input.GetKeyDown(KeyCode.S)){
			if (controller.Grounded){
				G_DIRECTION = GRAVITYDIRECTION.DOWN;
				controller.GoToStateSwitchGravity();
			}
		}

	}
}






















































































