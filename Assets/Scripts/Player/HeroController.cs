﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/* Hero Controller */
/* How the player controls the character. */
/* also controls his scale */
/* this also holds raycasts to stop him from going through walls */

public class HeroController : TomMono {

	public enum STATE{

		IDLE = 0,
		RUNNING = 1,
		SWITCH_GRAVITY = 2,
		MIDAIR = 3,
		LAND = 4,
		WIN = 5,
		DIE = 6
	}
	public STATE state;
	public int GetState(){
		return (int)state;
	}
	public enum MOVEDIRECTION{
		NONE,
		UP,
		DOWN,
		LEFT,
		RIGHT
	}
	public MOVEDIRECTION moveDirection;

	public enum MOVERESOLVE{
		NONE = 0,
		GAIN = 1,
		LOSS = -1
	}
	public MOVERESOLVE moveResolve;

	[System.Serializable]
	public class Sound{

		public AudioClip clip;
		[Range(0,100)]
		public float volume;
	}
	[Group("Sounds")]
	public Sound run;
	[Group("Sounds")]
	public Sound jump;
	[Group("Sounds")]
	public Sound land;

	//Movement
	private Vector3 InitialTouchLocation;
	[Group("Controls")]
	public float NeededTouchOffset = 0.1f;

	[Group("Physics")]
	public float speed = 1.0f;
	[Group("Physics")]
	public float rayDistance = 0.08f;
	[Group("Physics")]
	public bool Grounded = true;

	[HideInInspector]
	public bool isInBackground = false;
	private bool scaleTo = false;
	private Vector3 desiredLayerScale;

	[Group("Physics")]
	public float scaleTime = 1f;

	public bool updateEnabled = true;

	private HeroPhysics physics;

	private Dictionary<string, Dictionary<int,Vector3>> DesiredScale
		= new Dictionary<string, Dictionary<int,Vector3>>();

	private Dictionary<string,Vector2> RayDirection
		= new Dictionary<string, Vector2>();

	private string gravDirection;

	//run audio
	[HideInInspector]
	public float runMaxTime = 2f;
	private bool canplayRunclip = true;

	[Group("Editor")]
	public bool debugTouchControls = false;
	[Group("Editor")]
	public bool drawGizmos = true;

	//controls
	private bool moveRight = false;
	private bool moveLeft = false;
	private bool moveUp = false;
	private bool moveDown = false;
	
	private int moveTouch; /* the touch index that is moving the player */

	private void Start(){

		physics =GetComponent<HeroPhysics>();

		Vector3 neg = new Vector3(-1,1,1);
		Vector3 pos = new Vector3(1,1,1);
		AddToScale(GRAVITYDIRECTION.DOWN.ToString(),neg,pos);
		AddToScale(GRAVITYDIRECTION.UP.ToString(),pos,neg);
		AddToScale(GRAVITYDIRECTION.LEFT.ToString(),pos,neg);
		AddToScale(GRAVITYDIRECTION.RIGHT.ToString(),neg,pos);

		RayDirection.Add(MOVEDIRECTION.NONE.ToString(),Vector2.zero);
		RayDirection.Add(MOVEDIRECTION.UP.ToString(),Vector2.up);
		RayDirection.Add(MOVEDIRECTION.DOWN.ToString(),-Vector2.up);
		RayDirection.Add(MOVEDIRECTION.RIGHT.ToString(),Vector2.right);
		RayDirection.Add(MOVEDIRECTION.LEFT.ToString(),-Vector2.right);

	}

	IEnumerator RunClipTimer(){
		yield return new WaitForSeconds(runMaxTime);
		canplayRunclip = true;
	}

	#region State Functions

	public void GoToStateIdle(){
		if (Grounded)
			state = STATE.IDLE;
	}

	public void GoToStateRunning(){
		if (Grounded){
			if (canplayRunclip){
				Vector3 clipLocation = Vector3.zero;
				clipLocation.z = -100 + run.volume;
				if (run.clip)
					AudioSource.PlayClipAtPoint(run.clip,clipLocation);
				canplayRunclip = false;
				StartCoroutine("RunClipTimer");
			}
			state = STATE.RUNNING;
		}
	}

	public void GoToStateSwitchGravity(){
		Grounded = false;
		state = STATE.SWITCH_GRAVITY;
		Vector3 clipLocation = Vector3.zero;
		clipLocation.z = -100 + jump.volume;
		if (jump.clip)
			AudioSource.PlayClipAtPoint(jump.clip,clipLocation);
		
	}

	public void GoToStateMidair(){
		Grounded = false;
		state = STATE.MIDAIR;
	}

	public void GoToStateLand(){
		Grounded = true;
		state = STATE.LAND;
		Vector3 clipLocation = Vector3.zero;
		clipLocation.z = -100 + land.volume;
		if (land.clip)
			AudioSource.PlayClipAtPoint(land.clip, clipLocation);

	}

	public void GoToStateWin(){
		state = STATE.WIN;
	}

	public void GoToStateDie(){
		state = STATE.DIE;
	}

	#endregion


	#region Update Controls

	private void Update(){

		if (updateEnabled){
			gravDirection = physics.G_DIRECTION.ToString();

			if (Application.isEditor && !debugTouchControls)
				EditorControls();
			else
				TouchControls();

			UpdateMovement();
			CheckForCollisions();
		}

		if (scaleTo){

			float step = scaleTime * Time.deltaTime;
			transform.localScale = Vector3.MoveTowards(transform.localScale,desiredLayerScale,step);
			float distance = Vector3.Distance(transform.localScale,desiredLayerScale);
			if (distance < 0.001f){
				transform.localScale = desiredLayerScale;
				scaleTo = false;
				updateEnabled = true;
			}
		}
	}

	public void MoveLayers(){

		if (isInBackground){
			isInBackground = false;
			scaleTo = true;
			updateEnabled = false;
			desiredLayerScale = transform.localScale * 2f;
			physics.GotoFrontLayer();
		}
		else{
			isInBackground = true;
			scaleTo = true;
			updateEnabled = false;
			desiredLayerScale = transform.localScale * 0.5f;
			physics.GoToBackLayer();
		}
	}
	private void CheckForCollisions(){
		string  mDirection= moveDirection.ToString();
		RaycastHit2D hit = Physics2D.Raycast((Vector2)transform.position,RayDirection[mDirection],rayDistance,physics.floorMask);
		if (!hit.transform){ // we can move
			//if (state != STATE.LAND)
				Move (mDirection);
		}
		else{ // we cannot move

		}
	}

	private void Move(string mDirection){
		int mValue = (int)moveResolve;
		Vector3 scale = GetScale(gravDirection,mValue);
		if (isInBackground)
			scale = scale * 0.5f;

		if (scale != Vector3.zero){
			transform.localScale = scale;
			transform.position += ((Vector3)RayDirection[mDirection] * speed) * Time.deltaTime;
			GoToStateRunning();
		}
		else{
			GoToStateIdle();
		}
	}

	public void MoveRight(){
		moveLeft = false;
		moveDown = false;
		moveUp = false;
		moveRight = true;
	}

	public void MoveLeft(){
		moveRight = false;
		moveUp = false;
		moveDown = false;
		moveLeft = true;
	}

	public void MoveUp(){
		moveRight = false;
		moveLeft = false;
		moveDown = false;
		moveUp = true;
	}

	public void MoveDown(){
		moveRight = false;
		moveLeft = false;
		moveUp = false;
		moveDown = true;
	}

	public void CancelMove(){

		moveRight = false;
		moveLeft = false;
		moveUp = false;
		moveDown = false;
		InitialTouchLocation = Vector3.zero;
	}

	public void TouchControls(){
		int touchCount = Input.touchCount-1;
		if (touchCount+1 > 0 && Input.GetTouch(touchCount).phase == TouchPhase.Began){

			if (InitialTouchLocation == Vector3.zero){
				InitialTouchLocation = Input.GetTouch(touchCount).position;
				moveTouch = touchCount;
			}

			if ( Input.GetTouch(touchCount).position.x >= (Screen.width / 2)){
				if (Grounded)
					physics.ChangeDirection();
			}

		}

		if (touchCount+1 > 0 && Input.GetTouch(moveTouch).phase == TouchPhase.Moved){

			Vector3 touchPos = Input.GetTouch(moveTouch).position;
			float distance = Vector3.Distance(touchPos,InitialTouchLocation);
			if (distance > NeededTouchOffset){
				if (physics.G_DIRECTION == GRAVITYDIRECTION.DOWN || physics.G_DIRECTION == GRAVITYDIRECTION.UP){
					if (touchPos.x > InitialTouchLocation.x)
						MoveRight();
					else if (touchPos.x < InitialTouchLocation.x)
						MoveLeft();
				}
				if (physics.G_DIRECTION == GRAVITYDIRECTION.RIGHT || physics.G_DIRECTION == GRAVITYDIRECTION.LEFT){

					if (touchPos.y > InitialTouchLocation.y)
						MoveUp();
					else if (touchPos.y < InitialTouchLocation.y)
						MoveDown();
				}
			}
		}

		if (Input.touchCount == 0){
			CancelMove();
		}
	}

	public void UpdateMovement(){

		if (physics.G_DIRECTION == GRAVITYDIRECTION.DOWN || physics.G_DIRECTION == GRAVITYDIRECTION.UP){
			if (moveRight){
				moveResolve = MOVERESOLVE.GAIN;
				moveDirection = MOVEDIRECTION.RIGHT;

			}
			else if (moveLeft){
				moveResolve = MOVERESOLVE.LOSS;
				moveDirection = MOVEDIRECTION.LEFT;
			}
			else{ // idle
				moveResolve = MOVERESOLVE.NONE;
				moveDirection = MOVEDIRECTION.NONE;
			}
		}
		
		if (physics.G_DIRECTION == GRAVITYDIRECTION.RIGHT || physics.G_DIRECTION == GRAVITYDIRECTION.LEFT){
			if (moveUp){
				moveResolve = MOVERESOLVE.GAIN;
				moveDirection = MOVEDIRECTION.UP;
			}
			else if (moveDown){
				moveResolve = MOVERESOLVE.LOSS;
				moveDirection = MOVEDIRECTION.DOWN;
			}
			else{ // idle
				moveResolve = MOVERESOLVE.NONE;
				moveDirection = MOVEDIRECTION.NONE;
			}
		}
	}
	private void EditorControls(){


		if (physics.G_DIRECTION == GRAVITYDIRECTION.DOWN || physics.G_DIRECTION == GRAVITYDIRECTION.UP){
			if (Input.GetKey(KeyCode.RightArrow)){
				MoveRight();
			}
			else if (Input.GetKey(KeyCode.LeftArrow)){
				MoveLeft();
			}
			else{
				CancelMove();
			}
		}

		if (physics.G_DIRECTION == GRAVITYDIRECTION.RIGHT || physics.G_DIRECTION == GRAVITYDIRECTION.LEFT){
			if (Input.GetKey(KeyCode.UpArrow)){
				MoveUp();
			}
			else if (Input.GetKey(KeyCode.DownArrow)){
				MoveDown();
			}
			else{
				CancelMove();
			}

		}
	}

	#endregion


	#region Helpers
	
	private void AddToScale(string gravD, Vector3 gain, Vector3 loss){
		Dictionary<int,Vector3> dic = new Dictionary<int, Vector3>();
		dic.Add(1,gain);
		dic.Add(-1,loss);
		DesiredScale.Add(gravD,dic);
	}
	
	private Vector3 GetScale(string _gravD, int _moveResolve){
		
		Dictionary<int, Vector3> v; 
		
		if (DesiredScale.TryGetValue(_gravD, out v)){
			
			Vector3 outVector;
			if (v.TryGetValue(_moveResolve,out outVector)){
				return outVector;
			}
			
			return Vector3.zero;
		}
		
		return Vector3.zero; // returning zero value so we can check it. if it is zero it means that you arent moving. We should go into an idle wait 
	}
	
	#endregion


	#region ONDRAWGIZMOS
	
	void OnDrawGizmos(){
		if (drawGizmos){
			Transform pivot = transform.FindChild("Pivot");
			if (pivot){
				Gizmos.color = Color.red;
				Gizmos.DrawWireCube(pivot.position,new Vector3((Camera.main.orthographicSize * Camera.main.aspect) * 2f,Camera.main.orthographicSize * 2f,Camera.main.orthographicSize));
			}
		}
	}
	#endregion



}


















































































































