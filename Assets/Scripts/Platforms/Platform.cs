﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class Platform : TomMono {

	protected bool activated;

	public bool doesReactivate = true;
	public float secondsToReactivate;

	private void Start(){
		DoStart();
	}
	protected virtual void DoStart(){

	}


	public virtual void DoAction(HeroController controller, HeroPhysics physics, HeroCollision collision){
		StartCoroutine("ReactivateAfterSeconds");
	}

	protected IEnumerator ReactivateAfterSeconds(){
		activated = true;
		yield return new WaitForSeconds(secondsToReactivate);
		if (doesReactivate){
			activated = false;
		}
	}
}
