﻿using UnityEngine;
using System.Collections;

public class SwitchLayersPlatform : Platform {

	public override void DoAction (HeroController controller, HeroPhysics physics, HeroCollision collision)
	{
		if (!activated)
			controller.MoveLayers();
		base.DoAction (controller, physics, collision);


	}
}
