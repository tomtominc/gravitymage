﻿using UnityEngine;
using System.Collections;

public class WinPlatform : Platform {

	public bool allDirectionsValid;
	public GRAVITYDIRECTION ValidGravityDirection;

	public override void DoAction (HeroController controller, HeroPhysics physics, HeroCollision collision)
	{
		if ((physics.G_DIRECTION == ValidGravityDirection) || allDirectionsValid){
			controller.updateEnabled = false;
			collision.HeroWin();
			print ("I win!");
		}
	}

}
