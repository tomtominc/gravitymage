﻿using UnityEngine;
using System.Collections;

public class SpellPlatform : Platform {
	
	public override void DoAction (HeroController controller, HeroPhysics physics, HeroCollision collision)
	{
		if (!activated){
			base.DoAction(controller,physics,collision);
			physics.ChangeGravityOrientation ();
		}
	}


}
