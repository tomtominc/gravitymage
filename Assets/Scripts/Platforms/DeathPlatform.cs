﻿using UnityEngine;
using System.Collections;

public class DeathPlatform : Platform {


	protected override void DoStart ()
	{
		BoxCollider2D boxcollider2D = GetComponent<BoxCollider2D>();
		boxcollider2D.offset = new Vector2(0,-0.5f);
		boxcollider2D.size = new Vector2(1f,0.5f);
	}
	public override void DoAction (HeroController controller, HeroPhysics physics, HeroCollision collision)
	{
		controller.updateEnabled = false;
		physics.updateEnabled = false;
		collision.HeroDie();

	}
}
