﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManager : TomMono {

	public static GameManager instance;
	public bool theOnlyInstance = false;
	public float GRAVITY = 9.8f;
	public string ENVIORNMENT = "Cave";
	public int LEVELNUMBER = 0;

	public void GoToNextLevel(){
		LEVELNUMBER++;
		if (Application.CanStreamedLevelBeLoaded(ENVIORNMENT + LEVELNUMBER.ToString())){
			Application.LoadLevel(ENVIORNMENT + LEVELNUMBER.ToString());
		}
		else{
			LEVELNUMBER = 1;
			Application.LoadLevel(ENVIORNMENT + LEVELNUMBER.ToString());
		}
	}

	public void SetEnviornment(string _evo){
		ENVIORNMENT = _evo;
	}

	public void LoadLevel(int levelNum){
		LEVELNUMBER = levelNum;
		if (Application.CanStreamedLevelBeLoaded(ENVIORNMENT + LEVELNUMBER.ToString())){
			Application.LoadLevel(ENVIORNMENT + LEVELNUMBER.ToString());
		}
		else{
			LEVELNUMBER = 1;
			Application.LoadLevel(ENVIORNMENT + LEVELNUMBER.ToString());
		}
	}

	public void LoadLevel(Button b){
		Text t = b.transform.FindChild("Text").GetComponent<Text>();
		LEVELNUMBER = System.Convert.ToInt32( t.text);

		if (Application.CanStreamedLevelBeLoaded(ENVIORNMENT + t.text)){
			Application.LoadLevel(ENVIORNMENT + t.text);
		}
		else{
			print ("cant load level "+ ENVIORNMENT + t.text);
		}
	}

	void Awake(){
		if (instance == null){
			instance = this;
			theOnlyInstance = true;

		}

		if (!theOnlyInstance)
			Destroy(gameObject);

		DontDestroyOnLoad(this);
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
